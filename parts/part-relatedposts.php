<!-- Marketing Strategy -->
<?php if (is_page('2239') || $post->post_parent == '2239'): ?>

<div class="container"><div class="divider"><h5><span>Latest Related Article</span></h5></div></div>

<?php
  $args = array(
    'posts_per_page' => 1,
    'tax_query' => array(
      array(
        'taxonomy' => 'category',
        'field' => 'id',
        'terms' => '70'
        )
      )
    );
  $listing_query = new WP_Query($args);
?>

<?php if ($listing_query->have_posts()) : while ($listing_query->have_posts()) : $listing_query->the_post(); ?>

<div class="container relatedposts">
  <a href="<?php the_permalink(); ?> "><?php the_post_thumbnail(); ?></a>
  <h5><a href="<?php the_permalink(); ?> "><?php the_title(); ?></a></h5>
</div>

<?php endwhile; endif; // end of taxonomy query loop. ?>

<?php wp_reset_postdata(); ?>

<?php endif; ?>





<!-- Websites -->
<?php if (is_page('2252') || $post->post_parent == '2252'): ?>

<div class="container"><div class="divider"><h5><span>Latest Related Article</span></h5></div></div>

<?php
  $args = array(
    'posts_per_page' => 1,
    'tax_query' => array(
      array(
        'taxonomy' => 'category',
        'field' => 'id',
        'terms' => ''
        )
      )
    );
  $listing_query = new WP_Query($args);
?>

<?php if ($listing_query->have_posts()) : while ($listing_query->have_posts()) : $listing_query->the_post(); ?>

<div class="container relatedposts">
  <a href="<?php the_permalink(); ?> "><?php the_post_thumbnail(); ?></a>
  <h5><a href="<?php the_permalink(); ?> "><?php the_title(); ?></a></h5>
</div>

<?php endwhile; endif; // end of taxonomy query loop. ?>

<?php wp_reset_postdata(); ?>

<?php endif; ?>







<!-- SEO -->
<?php if (is_page('2234') || $post->post_parent == '2234'): ?>

<div class="container"><div class="divider"><h5><span>Latest Related Article</span></h5></div></div>

<?php
  $args = array(
    'posts_per_page' => 1,
    'tax_query' => array(
      array(
        'taxonomy' => 'category',
        'field' => 'id',
        'terms' => '46'
        )
      )
    );
  $listing_query = new WP_Query($args);
?>

<?php if ($listing_query->have_posts()) : while ($listing_query->have_posts()) : $listing_query->the_post(); ?>

<div class="container relatedposts">
  <a href="<?php the_permalink(); ?> "><?php the_post_thumbnail(); ?></a>
  <h5><a href="<?php the_permalink(); ?> "><?php the_title(); ?></a></h5>
</div>

<?php endwhile; endif; // end of taxonomy query loop. ?>

<?php wp_reset_postdata(); ?>

<?php endif; ?>



<!-- Content Marketing -->
<?php if (is_page('2383') || $post->post_parent == '2383'): ?>

<div class="container"><div class="divider"><h5><span>Latest Related Article</span></h5></div></div>

<?php
  $args = array(
    'posts_per_page' => 1,
    'tax_query' => array(
      array(
        'taxonomy' => 'category',
        'field' => 'id',
        'terms' => ''
        )
      )
    );
  $listing_query = new WP_Query($args);
?>

<?php if ($listing_query->have_posts()) : while ($listing_query->have_posts()) : $listing_query->the_post(); ?>

<div class="container relatedposts">
  <a href="<?php the_permalink(); ?> "><?php the_post_thumbnail(); ?></a>
  <h5><a href="<?php the_permalink(); ?> "><?php the_title(); ?></a></h5>
</div>

<?php endwhile; endif; // end of taxonomy query loop. ?>

<?php wp_reset_postdata(); ?>

<?php endif; ?>


<!-- Online Advertising -->
<?php if (is_page('2254') || $post->post_parent == '2254'): ?>

<div class="container"><div class="divider"><h5><span>Latest Related Article</span></h5></div></div>

<?php
  $args = array(
    'posts_per_page' => 1,
    'tax_query' => array(
      array(
        'taxonomy' => 'category',
        'field' => 'id',
        'terms' => ''
        )
      )
    );
  $listing_query = new WP_Query($args);
?>

<?php if ($listing_query->have_posts()) : while ($listing_query->have_posts()) : $listing_query->the_post(); ?>

<div class="container relatedposts">
  <a href="<?php the_permalink(); ?> "><?php the_post_thumbnail(); ?></a>
  <h5><a href="<?php the_permalink(); ?> "><?php the_title(); ?></a></h5>
</div>

<?php endwhile; endif; // end of taxonomy query loop. ?>

<?php wp_reset_postdata(); ?>

<?php endif; ?>


<!-- Social Media -->
<?php if (is_page('2204') || $post->post_parent == '2204'): ?>

<div class="container"><div class="divider"><h5><span>Latest Related Article</span></h5></div></div>

<?php
  $args = array(
    'posts_per_page' => 1,
    'tax_query' => array(
      array(
        'taxonomy' => 'category',
        'field' => 'id',
        'terms' => '13'
        )
      )
    );
  $listing_query = new WP_Query($args);
?>

<?php if ($listing_query->have_posts()) : while ($listing_query->have_posts()) : $listing_query->the_post(); ?>

<div class="container relatedposts">
  <a href="<?php the_permalink(); ?> "><?php the_post_thumbnail(); ?></a>
  <h5><a href="<?php the_permalink(); ?> "><?php the_title(); ?></a></h5>
</div>

<?php endwhile; endif; // end of taxonomy query loop. ?>

<?php wp_reset_postdata(); ?>

<?php endif; ?>

<!-- Email Marketing -->
<?php if (is_page('2255') || $post->post_parent == '2255'): ?>

<div class="container"><div class="divider"><h5><span>Latest Related Article</span></h5></div></div>

<?php
  $args = array(
    'posts_per_page' => 1,
    'tax_query' => array(
      array(
        'taxonomy' => 'category',
        'field' => 'id',
        'terms' => '49'
        )
      )
    );
  $listing_query = new WP_Query($args);
?>

<?php if ($listing_query->have_posts()) : while ($listing_query->have_posts()) : $listing_query->the_post(); ?>

<div class="container relatedposts">
  <a href="<?php the_permalink(); ?> "><?php the_post_thumbnail(); ?></a>
  <h5><a href="<?php the_permalink(); ?> "><?php the_title(); ?></a></h5>
</div>

<?php endwhile; endif; // end of taxonomy query loop. ?>

<?php wp_reset_postdata(); ?>

<?php endif; ?>



<!-- Traditional Advertising -->
<?php if (is_page('2205') || $post->post_parent == '2205'): ?>

<div class="container"><div class="divider"><h5><span>Latest Related Article</span></h5></div></div>

<?php
  $args = array(
    'posts_per_page' => 1,
    'tax_query' => array(
      array(
        'taxonomy' => 'category',
        'field' => 'id',
        'terms' => ''
        )
      )
    );
  $listing_query = new WP_Query($args);
?>

<?php if ($listing_query->have_posts()) : while ($listing_query->have_posts()) : $listing_query->the_post(); ?>

<div class="container relatedposts">
  <a href="<?php the_permalink(); ?> "><?php the_post_thumbnail(); ?></a>
  <h5><a href="<?php the_permalink(); ?> "><?php the_title(); ?></a></h5>
</div>

<?php endwhile; endif; // end of taxonomy query loop. ?>

<?php wp_reset_postdata(); ?>

<?php endif; ?>


<!-- Public Relations-->
<?php if (is_page('2256') || $post->post_parent == '2256'): ?>

<div class="container"><div class="divider"><h5><span>Latest Related Article</span></h5></div></div>

<?php
  $args = array(
    'posts_per_page' => 1,
    'tax_query' => array(
      array(
        'taxonomy' => 'category',
        'field' => 'id',
        'terms' => ''
        )
      )
    );
  $listing_query = new WP_Query($args);
?>

<?php if ($listing_query->have_posts()) : while ($listing_query->have_posts()) : $listing_query->the_post(); ?>

<div class="container relatedposts">
  <a href="<?php the_permalink(); ?> "><?php the_post_thumbnail(); ?></a>
  <h5><a href="<?php the_permalink(); ?> "><?php the_title(); ?></a></h5>
</div>

<?php endwhile; endif; // end of taxonomy query loop. ?>

<?php wp_reset_postdata(); ?>

<?php endif; ?>
