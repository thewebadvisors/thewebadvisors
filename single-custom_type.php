<?php
/*
This is the custom post type post template.
If you edit the post type name, you've got
to change the name of this template to
reflect that name change.

i.e. if your custom post type is called
register_post_type( 'bookmarks',
then your single template should be
single-bookmarks.php

*/
?>

<?php get_header(); ?>
		
		<div class="main-content">
		
			<div class="inner-content  container  clearfix">
		
			    <div class="primary-content  clearfix" role="main">

				    <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
				
				    <article <?php post_class('clearfix'); ?> role="article">
										
					    <section class="entry-content  clearfix">
						
							<h1 class="single-title  custom-post-type-title"><?php the_title(); ?></h1>
						    <?php the_content(); ?>
				
					    </section> <!-- /article section -->
				
				    </article> <!-- /article -->
				
				    <?php endwhile; endif; ?> <!-- /end main loop (if/while) -->
		
			    </div> <!-- /primary-content -->

			    <?php get_sidebar(); // sidebar ?>
			    
			</div> <!-- /inner-content -->

		</div> <!-- /main-content -->

<?php get_footer(); ?>