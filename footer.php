	<div class="lower-cta">
		<div class="container">
			<div style="height:100%;">
				<a class="button white magnific-form" href="#form-popup">Let's Collaborate</a>
			</div>

				<div id="form-popup" class="white-popup-block mfp-hide">
					<?php echo do_shortcode('[gravityform id="2" title="false" description="false" ajax="true"]') ?>
				</div>
		</div>
	</div>

	<footer class="page-footer " role="contentinfo">

		<div class="inner-footer container clearfix">


			<div class="row">
				<div class="col-md-4 footer-services clearfix">
					<h3>Our Services</h3>
					<a href="/marketing-strategy"><span title="Marketing Strategy" class="tooltip icon-marketing-strategy "></span></a>
					<a href="/websites"><span title="Websites" class="tooltip icon-websites"></span></a>
					<a href="/seo"><span title="SEO" class="tooltip icon-seo"></span></a>

					<a href="/content-marketing"><span title="Content Marketing" class="tooltip icon-content-marketing2 "></span></a>
					<a href="/online-advertising"><span title="Online Advertising" class="tooltip icon-roe"></span></a>
					<a href="/social-media"><span title="Social Media" class="tooltip icon-social"></span></a>

					<a href="/email-marketing"><span title="Email Marketing" class="tooltip icon-email-marketing2 "></span></a>
					<a href="/traditional-advertising"><span title="Traditional Marketing" class="tooltip icon-press"></span></a>
						<a href="/public-relations"><span title="Public Relations" class="tooltip icon-public-relations "></span></a>


					<!-- <a href=""><span class="icon-social"></span></a>
					<a href=""><span class="icon-seo"></span></a>

					<a href="#"><span title="test" class="tooltip icon-roe"></span></a>
					<a href=""><span class="icon-social"></span></a>
					<a href=""><span class="icon-seo"></span></a> -->




				</div>

				<div class="col-md-4 footer-testimonials">
					<h3>Recent Testimonials</h3>

					<div class="">
						<?php if( have_rows('testimonials', 'options') ): ?>
							<?php while( have_rows('testimonials', 'options') ): the_row(); ?>

								<ul>
									<li class="testimonial">	<?php echo get_sub_field('footer_testimonial_content', 'options'); ?></li>
								</ul>

							<?php endwhile; ?>
						<?php endif; ?>

					</div>

				</div>

				<div class="col-md-4">
					<h3>Get Marketing Tips &amp; Tricks</h3>
					<nav class="nav-footer" role="navigation">
						<?php echo do_shortcode('[gravityform id="3" title="false" description="false" ajax="true"]') ?>
					</nav>
				</div>
			</div>

			</div> <!-- /inner-footer -->

			<div class="lower-footer">

				<div class="container">
					<div class="row clearfix">
						<div class="col-md-6 copyright">&copy; <?php echo date('Y'); ?> <?php bloginfo('name'); ?>.</div>
						<div class="col-md-6">
							<p itemprop="telephone" class="phone-number">888-414-5463</p>
							<div class="social-icons">
								<a target="blank" href="https://www.facebook.com/TheWebAdvisors"><i class="icon-facebook"></i></a>
								<a target="blank" href="https://twitter.com/thewebadvisors"><i class="icon-twitter"></i></a>
								<a target="blank" href="https://www.linkedin.com/company/the-web-advisors"><i class="icon-linkedin"></i></a>
								<a target="blank" href="https://www.instagram.com/thewebadvisors/"><i class="icon-instagram"></i></a>
								<a target="blank" href="https://plus.google.com/+TheWebAdvisorsCa"><i class="icon-google-plus"></i></a>
							</div>
						</div>
					</div>
				</div>

			</div>






	</footer> <!-- /footer -->

	<!-- all js scripts are loaded in library/bones.php -->
	<?php wp_footer(); ?>
</div>
</body>

</html> <!-- end page. what a ride! -->
