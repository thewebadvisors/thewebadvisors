# BAREBONES DEVELOPER THEME

This developer focused theme takes advantage of the great Bones theme, by Eddie Machado. However it has been customized to streamline development in our environment. BareBones is lean but strong.

********************

## CHANGELOG

`v1.27`

* created widget plugins (plain widgets essentially) to be used as base code for tying in ACFs into widgets
* added extra wrapper to widget so you can give them internal padding while floating / gridding the outside wrap

`v1.26`

* updated [modified list pages](http://www.michaeldozark.com/tips/even-better-includes-and-excludes-for-wp_list_pages/) function added to helpers; new function name now too
* removed support for non-responsive setup -- fixed width sites are dead!
* favicon tweaking (why can't browsers and OS's just support SVG!)
* started a .gitignore file, likely needs tweaking

`v1.25`

* added new tabbed pager slider helper
* fixed some cycle slider helpers
* continued code clean up and tweaking

`v1.24`

* added [Magnific Popup](http://dimsemenov.com/plugins/magnific-popup/) to BB
* related tweaks to support Magnific Popup

`v1.23`

* ACF widget helper awesomeness
* default grid edits
* rename wrap to container
* old helpers clean up
* minor tweaks and fixes
* known bug: with visualblocks active, no other items can be added to TinyMCE menu (e.g. MCE Table Button does not show if plugin installed)

`v1.21`  

* updated visualblocks plugin (fixed it since it was broken)
* removed hyphens from all p elements
* removed link to Chrome Frame, program no longer exists
* fixed IE conditional stylesheet bug...reluctantly
* default lightbox is now MagnificPopup

`v1.20`  

* major-ish revamp!
* revised grid/column setup and shortcode
* css clean up and better class naming begins using these [best practices](https://github.com/csswizardry/CSS-Guidelines)
* rem's starting to sneak into core styling
* implemented some better [preset font styling](http://csswizardry.com/2012/02/pragmatic-practical-font-sizing-in-css/) 

`v1.13`  

* new css3 drop down menu styling with UI friendly fade effect
* cycle2 helpers (slideshow & carousel)
* misc tweaks in prep for some more major changes upcoming
* various tweaks to CSS and code clean up

`v1.11`  

* integrated WP Generated Styles into base.css for responsive builds
* removed usage of transients for now as it caches current menu class
* misc tweaks

`v1.09`  

* new menu functions, using transients to significant reduce server load and increase site load speed
* syncronize normalize.css with the Bones version, plus some tweaks
* beta multiple single image gallery setup (does not fully work yet)

`v1.08`  

* function added to functions.php enabling you to have custom thumbnail sizes in media dropdown box
* improved page layout setup for fixed width sidebars and fluid content
* added page layout selectors to 768px media query file
* removed ID selectors from CSS; removed IDs from template files
* favicon fix for Firefox bug

`v1.05`  

* name change to BareBones (thanks to Nick Murray at Mountain Air web for new logo)
* responsive css improved
* updated slider to new multi slider setup (using ACF Repeater instead of custom query)

`v1.04`  

* edits to bring theme up to speed with WP 3.8 (font icons for CPTs, TinyMCE edits, etc)
* new theme preview images

`v1.03`  

* CPT taxonomy fix
* misc tweaks
* new effects helder folder (intended to be used for minor JS stuff like a "scroll to top" script)
* more css clean up; misc tweaks

`v1.02`  

* new helpers: multitple slideshows; non visible lightbox gallery, accessed through text link
* misc css clean up
* new favicon setup, supporting super crazy HD (Android/Chrome)
* favicon now references in admin.php, so it loads properly in admin area and login screen
* improve function to add visualblocks to TinyMCE (no longer breaks Raw HTML plugin)

`v1.01`  

* upgraded [captions shortcode](http://iag.me/tech/12-hacks-and-tips-to-declutter-wordpress/) to be HTML5 compliant 
* new way of fixing WP's extra P tags around shortcodes that are on their own line
* misc css clean up (thank you Nick Murray)

`v1.00`  

* added TinyMCE visualblocks to the editor, hooray!
* customized layout and types buttons for TinyMCE
* revised CPT setup, using variables for singular & plural
* numerous changes to the original Bones theme have occured so I am making this a full new version

`v0.10`  

* I took the Bones Theme and started tweaking...