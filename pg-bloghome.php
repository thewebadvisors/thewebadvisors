<?php
/*
Template Name: Blog Home Page
*/
?>

<?php get_header(); ?>
		
	<div class="main-content">
	
		<div class="page-title-wrap" <?php if (get_field('title_bg_image')) { ?> style="background-image:url(<?php the_field('title_bg_image') ?>)"<?php } ?>>
			<div class="title-wrap-overlay" <?php if (get_field('title_bg_image')) { ?> style="background: rgba(0,0,0,0.4);"<?php } ?>>
				<div class="container">
					<h1 class="page-title"<?php if (get_field('title_bg_image')) { ?>style="color: #fff;"<?php } ?> ><?php the_title(); ?></h1>
					<?php if (get_field('page_subtitle')) { ?><h3 <?php if (get_field('title_bg_image')) { ?>style="color: #fff;"<?php } ?>><?php the_field('page_subtitle') ?></h3><?php } ?>
				</div>
			</div>
		</div>

		<div class="inner-content container clearfix">

			<div class="row">
	
			<div class="primary-content  col-md-7 clearfix" role="main">

				<?php
					$temp = $wp_query;
					$wp_query = null;
					$wp_query = new WP_Query( 
						array( 
								'posts_per_page' => 4, 
								'paged' => $paged
							) 
						);
				?>

				<?php if ($wp_query->have_posts()) : while ($wp_query->have_posts()) : $wp_query->the_post(); ?>
					
					<article <?php post_class('clearfix'); ?> role="article">

						<section class="entry-content  blog  clearfix" itemprop="articleBody">

							<a href="<?php the_permalink(); ?>"> <?php the_post_thumbnail('crop-700-400'); ?></a>

							<div class="row">
								<div class="col-sm-2 ">
									<div class="post-date"><span class="day"><?php the_time('d') ?></span><span class="month-year"><?php the_time('M y') ?></span></div>
									<div class="post-meta">
										<?php the_author_posts_link(); ?><?php comments_popup_link( 'Leave a Comment', '1 Comment', '% Comments' ); ?>
									</div> <!-- /post-meta -->
								</div>
								<div class="col-sm-10">
									<div class="category-list clearfix"><?php the_category(" "); ?></div>
									<h2 class="single-title" itemprop="headline"><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h2>
									<?php the_excerpt(); ?>
								</div>
							</div>
							

							<!-- 
							<?php global $more; $more = 0; ?>
							<?php the_content("Read More..."); ?>
							-->

							

						</section> <!-- /blog content section -->
				
					</article> <!-- /article -->

				<?php endwhile; endif; ?>

				<?php /* Display navigation to next/previous pages when applicable */ ?>
				<?php if ( $wp_query->max_num_pages > 1 ) : ?>
					<?php $max_page = $wp_query->max_num_pages; ?>
					<nav class="wp-prev-next">
						<ul class="clearfix">
							<li class="prev-link"><?php next_posts_link(__('&laquo; Older Entries', 'bonestheme')) ?></li>
							 <?php if (($paged < $max_page) && ($paged > 1))  { echo "<li class='nav-divider'><span>|</span></li>"; }  ?>  
							<li class="next-link"><?php previous_posts_link(__('Newer Entries &raquo;', 'bonestheme')) ?></li>
						</ul>
					</nav>
				<?php endif; ?>

				<?php $wp_query = null; $wp_query = $temp; ?>
				<?php wp_reset_postdata(); ?>
	
			</div> <!-- /primary-content -->

			<div class="col-md-4 col-md-offset-1 ">
			<?php get_sidebar(); // sidebar ?>
			</div>

			</div><!-- /row-->
			
		</div> <!-- /inner-content -->

	</div> <!-- /main-content -->

<?php get_footer(); ?>