<?php
/* Welcome to Bones :)
This is the core Bones file where most of the
main functions & features reside. If you have
any custom functions, it's best to put them
in the functions.php file.

Developed by: Eddie Machado
URL: http://themble.com/bones/
*/

/*********************
LAUNCH BONES
Let's fire off all the functions
and tools. I put it up here so it's
right up top and clean.
*********************/

// we're firing all out initial functions at the start
add_action('after_setup_theme','bones_ahoy', 16);

function bones_ahoy() {
    // launching operation cleanup
    add_action( 'init', 'bones_head_cleanup' );
    // remove WP version from RSS
    add_filter( 'the_generator', 'bones_rss_version' );
    // remove pesky injected css for recent comments widget
    add_filter( 'wp_head', 'bones_remove_wp_widget_recent_comments_style', 1 );
    // clean up comment styles in the head
    add_action( 'wp_head', 'bones_remove_recent_comments_style', 1 );
    // clean up gallery output in wp
    add_filter( 'gallery_style', 'bones_gallery_style' );

    // enqueue base scripts and styles
    add_action( 'wp_enqueue_scripts', 'bones_scripts_and_styles', 999 );

    // launching this stuff after theme setup
    bones_theme_support();

    // adding sidebars to Wordpress (these are created in functions.php)
    add_action( 'widgets_init', 'bones_register_sidebars' );
    // adding the bones search form (created in functions.php)
    add_filter( 'get_search_form', 'bones_wpsearch' );

    // cleaning up random code around images
    add_filter( 'the_content', 'bones_filter_ptags_on_images' );
    // cleaning up excerpt
    add_filter( 'excerpt_more', 'bones_excerpt_more' );

} /* end bones ahoy */

/*********************
WP_HEAD GOODNESS
The default wordpress head is
a mess. Let's clean it up by
removing all the junk we don't
need.
*********************/

function bones_head_cleanup() {
    // category feeds
    // remove_action( 'wp_head', 'feed_links_extra', 3 );
    // post and comment feeds
    // remove_action( 'wp_head', 'feed_links', 2 );
    // EditURI link
    remove_action( 'wp_head', 'rsd_link' );
    // windows live writer
    remove_action( 'wp_head', 'wlwmanifest_link' );
    // index link
    remove_action( 'wp_head', 'index_rel_link' );
    // previous link
    remove_action( 'wp_head', 'parent_post_rel_link', 10, 0 );
    // start link
    remove_action( 'wp_head', 'start_post_rel_link', 10, 0 );
    // links for adjacent posts
    remove_action( 'wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0 );
    // WP version
    remove_action( 'wp_head', 'wp_generator' );
    // remove WP version from css
    add_filter( 'style_loader_src', 'bones_remove_wp_ver_css_js', 9999 );
    // remove Wp version from scripts
    add_filter( 'script_loader_src', 'bones_remove_wp_ver_css_js', 9999 );

} /* end bones head cleanup */

// remove WP version from RSS
function bones_rss_version() { return ''; }

// remove WP version from scripts
function bones_remove_wp_ver_css_js( $src ) {
    if ( strpos( $src, 'ver=' ) )
        $src = remove_query_arg( 'ver', $src );
    return $src;
}

// remove injected CSS for recent comments widget
function bones_remove_wp_widget_recent_comments_style() {
   if ( has_filter('wp_head', 'wp_widget_recent_comments_style') ) {
      remove_filter('wp_head', 'wp_widget_recent_comments_style' );
   }
}

// remove injected CSS from recent comments widget
function bones_remove_recent_comments_style() {
  global $wp_widget_factory;
  if (isset($wp_widget_factory->widgets['WP_Widget_Recent_Comments'])) {
    remove_action('wp_head', array($wp_widget_factory->widgets['WP_Widget_Recent_Comments'], 'recent_comments_style'));
  }
}

// remove injected CSS from gallery
function bones_gallery_style($css) {
  return preg_replace("!<style type='text/css'>(.*?)</style>!s", '', $css);
}


/*********************
SCRIPTS & ENQUEUING
*********************/

// loading modernizr and jquery, and reply script
function bones_scripts_and_styles() {

  global $wp_styles; // call global $wp_styles variable to add conditional wrapper around ie stylesheet the WordPress way

  if (!is_admin()) {

    // modernizr (without media query polyfill) + respond.js for media query polyfill
    wp_register_script( 'bones-modernizr', get_stylesheet_directory_uri() . '/library/js/modernizr.custom.min.js', array(), '2.6.2', false );
    wp_register_script( 'bones-respond', get_stylesheet_directory_uri() . '/library/js/respond.min.js', array(), '1.4.2', false );

    // register main stylesheets
    wp_register_style( 'bones-basestyle', get_stylesheet_directory_uri() . '/library/css/style.css', array(), '', 'all' );

    // other stylesheets (plugins, ie, etc)
    wp_register_style( 'bones-ie-only', get_stylesheet_directory_uri() . '/library/css/ie.css', array(), '' );
    wp_register_style( 'addon-css-magnificpop', get_stylesheet_directory_uri() . '/library/css/magnific-popup.css', array(), '', 'all' );
    wp_register_style( 'addon-css-mmenu', get_stylesheet_directory_uri() . '/library/css/jquery.mmenu.all.css', array(), '', 'all' );
    wp_register_style( 'addon-css-tooltip', get_stylesheet_directory_uri() . '/library/css/tooltipster.css', array(), '', 'all' );


    // comment reply script for threaded comments
    if ( is_singular() AND comments_open() AND (get_option('thread_comments') == 1)) {
      wp_enqueue_script( 'comment-reply' );
    }

    // adding main scripts file in the footer
    wp_register_script( 'bones-js', get_stylesheet_directory_uri() . '/library/js/scripts.js', array( 'jquery' ), '', true );

    // adding other scripts to the theme (footer)
   wp_register_script( 'addon-cycle', get_stylesheet_directory_uri() . '/library/js/jquery.cycle2.min.js', array(), '', true );
    wp_register_script( 'addon-magnificpop', get_stylesheet_directory_uri() . '/library/js/jquery.magnific-popup.min.js', array(), '', true );
    wp_register_script( 'addon-mmenu', get_stylesheet_directory_uri() . '/library/js/jquery.mmenu.min.all.js', array(), '', true );
    // wp_register_script( 'addon-mmenu-fixed', get_stylesheet_directory_uri() . '/library/js/jquery.mmenu.fixedelements.js', array(), '', true );
    wp_register_script( 'addon-tooltip', get_stylesheet_directory_uri() . '/library/js/jquery.tooltipster.min.js', array(), '', true );
    wp_register_script( 'addon-isotope', get_stylesheet_directory_uri() . '/library/js/isotope.pkgd.min.js', array(), '', true );


    // enqueue styles and scripts
    wp_enqueue_style( 'addon-css-magnificpop' );
    wp_enqueue_style( 'addon-css-mmenu' );
      wp_enqueue_style( 'addon-css-tooltip' );

    wp_enqueue_style( 'bones-basestyle' );
    wp_enqueue_script( 'bones-modernizr' );
    wp_enqueue_script( 'bones-respond' );

    wp_enqueue_style( 'bones-ie-only' );
    $wp_styles->add_data( 'bones-ie-only', 'conditional', 'lt IE 9' ); // add conditional wrapper around ie stylesheet

    wp_enqueue_script( 'jquery' );
   wp_enqueue_script( 'addon-cycle' );
    wp_enqueue_script( 'addon-magnificpop' );
    wp_enqueue_script( 'addon-mmenu' );
     wp_enqueue_script( 'addon-tooltip' );
     wp_enqueue_script( 'addon-isotope' );
    wp_enqueue_script( 'bones-js' );
  }
}


/*********************
THEME SUPPORT
*********************/

// Adding WP 3+ Functions & Theme Support
function bones_theme_support() {

	// wp thumbnails (sizes handled in functions.php)
	add_theme_support('post-thumbnails');

	// default thumb size
	set_post_thumbnail_size(150, 150, true);

  // HTML5 captions
  add_theme_support( 'html5', array( 'gallery', 'caption' ) );

	// rss thingy
	add_theme_support('automatic-feed-links');

	// to add header image support go here: http://themble.com/support/adding-header-background-image-support/

	// adding post format support
	/*add_theme_support( 'post-formats',
		array(
			'aside',             // title less blurb
			'gallery',           // gallery of images
			'link',              // quick link to other site
			'image',             // an image
			'quote',             // a quick quote
			'status',            // a Facebook like status update
			'video',             // video
			'audio',             // audio
			'chat'               // chat transcript
		)
	); */

	// wp menus
	add_theme_support( 'menus' );

  // registering wp3+ menus
  register_nav_menus(
    array(
      'main-nav' => 'Primary Menu',           // main nav menu
      'tertiary-nav' => 'Tertiary Menu',      // tertiary nav menu
      'footer-nav' => 'Footer Menu'           // secondary nav (in footer)
    )
  );
} /* end bones theme support section */


/*********************
MENUS & NAVIGATION
*********************/

// setup wp_nav_menu stuff
/* usage: <?php bbones_nav_menu( 'main-nav', 'menu-main' ); ?> */
function bbones_nav_menu( $theme_location, $class )
{
    if( has_nav_menu( $theme_location ) )
    {
      $menu = wp_nav_menu( array(
      'container' => false,
      'theme_location'  => $theme_location,
      'menu_class'  => $class . ' nav clearfix',
      'echo'  => 0,
      ) );
    }
  echo $menu;
}


// give us some nice body class names please
function pretty_body_class() {
  $term = get_queried_object();

  if (is_single()) {
    $cat = get_the_category();
  }

  if(!empty($cat)) {
    return $cat[0]->slug;
  } elseif(isset($term->slug)) {
    return $term->slug;
  } elseif(isset($term->page_name)) {
    return $term->page_name;
  } elseif(isset($term->post_name)) {
    return $term->post_name;
  } else {
    return;
  }
}


/*********************
RELATED POSTS FUNCTION
*********************/

// Related Posts Function (call using bones_related_posts(); )
function bones_related_posts() {
	echo '<ul id="bones-related-posts">';
	global $post;
	$tags = wp_get_post_tags($post->ID);
	if($tags) {
		foreach($tags as $tag) { $tag_arr .= $tag->slug . ','; }
        $args = array(
        	'tag' => $tag_arr,
        	'numberposts' => 5, /* you can change this to show more */
        	'post__not_in' => array($post->ID)
     	);
        $related_posts = get_posts($args);
        if($related_posts) {
        	foreach ($related_posts as $post) : setup_postdata($post); ?>
	           	<li class="related_post"><a href="<?php the_permalink() ?>" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></li>
	        <?php endforeach; }
	    else { ?>
            <li class="no_related_post">No Related Posts Yet!</li>
		<?php }
	}
	wp_reset_query();
	echo '</ul>';
} /* end bones related posts function */


/*********************
RANDOM CLEANUP ITEMS
*********************/

// remove the p from around imgs (http://css-tricks.com/snippets/wordpress/remove-paragraph-tags-from-around-images/)
function bones_filter_ptags_on_images($content){
   return preg_replace('/<p>\s*(<a .*>)?\s*(<img .* \/>)\s*(<\/a>)?\s*<\/p>/iU', '\1\2\3', $content);
}


// This removes the annoying […] to a Read More link
function bones_excerpt_more($more) {
	global $post;
	// edit here if you like
	return '...  <a class="excerpt-read-more" href="'. get_permalink($post->ID) . '" title="Read '.get_the_title($post->ID).'"><span class="button green">Read more &raquo;</span></a>';
}


// Sets the post excerpt length to set characters length below.
function multiple_excerpt_lengths($length) {
  if ( is_front_page() ) {
    return 65;
  }
  elseif ( is_author() ) {
    return 65;
  }
  else {
    return 55;
  }
}
add_filter( 'excerpt_length', 'multiple_excerpt_lengths' );


// Bones custom excerpt length
// USAGE: echo bones_excerpt(25);
function bones_excerpt($charlength) {
  $excerpt = get_the_excerpt();
  $charlength++;
  if(strlen($excerpt)>$charlength) {
  $subex = substr($excerpt,0,$charlength-5);
  $exwords = explode(" ",$subex);
  $excut = -(strlen($exwords[count($exwords)-1]));
  if($excut<0) {
    echo substr($subex,0,$excut);
  } else {
    echo $subex;
  }
    echo "...";
  } else {
    echo $excerpt;
  }
}


// Make Read More link go to top of page (not an anchor link)
function remove_more_jump_link($link) {
	$offset = strpos($link, '#more-');
	if ($offset) {
		$end = strpos($link, '"',$offset);
	}
	if ($end) {
		$link = substr_replace($link, '', $offset, $end-$offset);
	}
	return $link;
	}
add_filter('the_content_more_link', 'remove_more_jump_link');


?>
