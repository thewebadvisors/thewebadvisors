<?php get_header(); ?>

		<div class="main-content">

			<div class="inner-content clearfix">

				<div class="primary-content clearfix" role="main">

					<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

						<article <?php post_class('clearfix'); ?> role="article" itemscope itemtype="http://schema.org/BlogPosting">

							<header class="article-header">

								<div class="container">
									<div class="row">
										<div class="col-md-9">
											<p class="byline post-date-single"><span class="icon-ribbon"></span><time datetime="<?php the_time('Y-m-j'); ?>" pubdate><?php the_time('F jS, Y'); ?></time></p>
											<h1 class="single-title " itemprop="headline"><?php the_title(); ?></h1>
											<?php if (get_field('post_subtitle_text')) { ?><h2 class="blog-subtittle"><?php the_field('post_subtitle_text') ?></h2><?php } ?>
										</div>
										<div class="col-md-3">

											<div class="navigation">
												<div class="navigation-previous"><?php previous_post_link( '%link', '<p class="lable">Previous Post</p><span class="icon-arrow-left"></span>' ); ?></div>
											<div class="navigation-next"><?php next_post_link( '%link', '<p class="lable">Next Post</p><span class="icon-arrow-right"></span>' ); ?></div>
											</div>

										</div>
									</div>
								</div>


								<!-- Get the URL of the featured image and use as full width  -->
								<?php if (get_field('full_width_banner_image_2')) { ?>
								<div class="post-banner" style="background-image: url( '<?php the_field('full_width_banner_image_2') ?>');">  </div>
								<?php } ?>

							</header> <!-- /article header -->



								<section class="entry-content container clearfix" itemprop="articleBody">


								<?php if (get_field('use_page_builder') == "no" ) { ?><?php the_content(); ?><?php } ?>

								<?php if (get_field('use_page_builder') == "yes" ) { ?>
									<?php

									// check if the flexible content field has rows of data
									if( have_rows('page_builder') ):

									     // loop through the rows of data
									    while ( have_rows('page_builder') ) : the_row();

									        if( get_row_layout() == '1_column_layout' ):

									        	echo '<div class="row"><div class="col-md-12">';
									        		the_sub_field('full_width_layout');
									        	echo '</div></div>';

									        elseif( get_row_layout() == '2_column_layout' ):

									        	echo '<div class="row"><div class="col-md-6">';
									        		the_sub_field('column_1_of_2');
									        	echo '</div>';
									        	echo '<div class="col-md-6">';
									        		the_sub_field('column_2_of_2');
									        	echo '</div></div>';


									        elseif( get_row_layout() == '3_column_layout' ):

									        	echo '<div class="row"><div class="col-md-4">';
									        		the_sub_field('column_1_of_3');
									        	echo '</div>';
									        	echo '<div class="col-md-4">';
									        		the_sub_field('column_2_of_3');
									        	echo '</div>';
									        	echo '<div class="col-md-4">';
									        		the_sub_field('column_3_of_3');
									        	echo '</div></div>';

									        elseif( get_row_layout() == '13_layout' ):

									        	echo '<div class="row"><div class="col-md-4">';
									        		the_sub_field('column_1-3');
									        	echo '</div>';
									        	echo '<div class="col-md-8">';
									        		the_sub_field('column_2-3');
									        	echo '</div></div>';

									        elseif( get_row_layout() == '23_layout' ):

									        	echo '<div class="row"><div class="col-md-8">';
									        		the_sub_field('column_2-3b');
									        	echo '</div>';
									        	echo '<div class="col-md-4">';
									        		the_sub_field('column_1-3b');
									        	echo '</div></div>';

									        elseif( get_row_layout() == 'divider_text' ):

									        	echo '<div class="divider"><h5><span>';
									        		the_sub_field('divider_title');
									        	echo '</sapn></h5></div>';

									         elseif( get_row_layout() == 'parallax_bg_section' ):

									        	echo '<div class="parallax-wrap"><div class="parallax" style="background-image: url('. get_sub_field('parallax_image') .')"></div></div>';





									        endif;

									    endwhile;

									else :

									    // no layouts found

									endif;

									?>
								<?php } ?>


								</section> <!-- /article section -->




							<footer class="article-footer container">


								<?php the_tags('<p class="tags"><span class="tags-title">Tags:</span> ', ', ', '</p>'); ?>
								<!-- Go to www.addthis.com/dashboard to customize your tools -->
								<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-547a12170177124f" async="async"></script>
								<!-- Go to www.addthis.com/dashboard to customize your tools -->
								<div class="addthis_sharing_toolbox"></div>



							</footer> <!-- /article footer -->

							<div class="container">
								  <?php comments_template(); // comments should go inside the article element ?>
							</div>


						</article> <!-- /article -->

					<?php endwhile; else : ?>

						<article class="post-not-found  hentry  clearfix">
				    		<header class="article-header">
				    			<h1><?php _e("Oops, Post Not Found!", "bonestheme"); ?></h1>
				    		</header>
				    		<section class="entry-content">
				    			<p><?php _e("Uh Oh. Something is missing. Try double checking things.", "bonestheme"); ?></p>
				    		</section>
				    		<footer class="article-footer">
				    		    <p><?php _e("This is the error message in the single.php template.", "bonestheme"); ?></p>
				    		</footer>
						</article>

					<?php endif; ?>



				</div> <!-- /primary-content -->

			</div> <!-- /inner-content -->

		</div> <!-- /main-content -->

<?php get_footer(); ?>
