<?php
/*
This is the custom post type taxonomy template.
If you edit the custom taxonomy name, you've got
to change the name of this template to
reflect that name change.

i.e. if your custom taxonomy is called
register_taxonomy( 'shoes',
then your single template should be
taxonomy-shoes.php

*/
?>

<?php get_header(); ?>
			
		<div class="main-content">
		
			<div class="inner-content  container  clearfix">
		
			    <div class="primary-content  clearfix" role="main">
			
				    <h1 class="archive-title h2"><span><?php _e("Posts Categorized:", "bonestheme"); ?></span> <?php single_cat_title(); ?></h1>

				    <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
				
				    <article <?php post_class('clearfix'); ?> role="article">
					
					    <header class="article-header">
						
							<h3><a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></h3>
							<span class="date"><?php the_time('m.d.Y') ?></span>
					
					    </header> <!-- /article header -->
				
					    <section class="entry-content">
						    <?php the_excerpt(); ?>
				
					    </section> <!-- /article section -->
					
					    <footer class="article-footer">
						
					    </footer> <!-- /article footer -->
				
				    </article> <!-- /article -->
				
				    <?php endwhile; ?>	
	
		            <nav class="wp-prev-next">
		                <ul class="clearfix">
		        	        <li class="prev-link"><?php next_posts_link(__('&laquo; Older Entries', "bonestheme")) ?></li>
		        	        <li class="next-link"><?php previous_posts_link(__('Newer Entries &raquo;', "bonestheme")) ?></li>
		                </ul>
		            </nav>
				        
				    <?php else : ?>
				
    					<article class="post-not-found  hentry  clearfix">
    						<header class="article-header">
    							<h1><?php _e("Oops, Post Not Found!", "bonestheme"); ?></h1>
    						</header>
    						<section class="entry-content">
    							<p><?php _e("Uh Oh. Something is missing. Try double checking things.", "bonestheme"); ?></p>
    						</section>
    						<footer class="article-footer">
    						    <p><?php _e("This is the error message in the taxonomy-custom_cat.php template.", "bonestheme"); ?></p>
    						</footer>
    					</article>
				
				    <?php endif; ?>
		
			    </div> <!-- /primary-content -->

			    <?php get_sidebar(); ?>
			    
			</div> <!-- /inner-content -->

		</div> <!-- /main-content -->

<?php get_footer(); ?>