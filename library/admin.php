<?php
/*
This file handles the admin area and functions.
You can use this file to make changes to the
dashboard. Updates to this page are coming soon.
It's turned off by default, but you can call it
via the functions file.

Developed by: Eddie Machado
URL: http://themble.com/bones/

Special Thanks for code & inspiration to:
@jackmcconnell - http://www.voltronik.co.uk/
Digging into WP - http://digwp.com/2010/10/customize-wordpress-dashboard/

*/

/************* DASHBOARD WIDGETS *****************/

// disable default dashboard widgets
function disable_default_dashboard_widgets() {
	// remove_meta_box( 'dashboard_right_now', 'dashboard', 'core' );    // Right Now Widget
	remove_meta_box( 'dashboard_recent_comments', 'dashboard', 'core' ); // Comments Widget
	remove_meta_box( 'dashboard_incoming_links', 'dashboard', 'core' );  // Incoming Links Widget
	remove_meta_box( 'dashboard_plugins', 'dashboard', 'core' );         // Plugins Widget
	// remove_meta_box( 'dashboard_quick_press', 'dashboard', 'core' );  // Quick Press Widget
	remove_meta_box( 'dashboard_recent_drafts', 'dashboard', 'core' );   // Recent Drafts Widget
	remove_meta_box( 'dashboard_primary', 'dashboard', 'core' );         //
	remove_meta_box( 'dashboard_secondary', 'dashboard', 'core' );       //

	// removing plugin dashboard boxes
	remove_meta_box( 'yoast_db_widget', 'dashboard', 'normal' );         // Yoast's SEO Plugin Widget

	/*
	have more plugin widgets you'd like to remove?
	share them with us so we can get a list of
	the most commonly used. :D
	https://github.com/eddiemachado/bones/issues
	*/
}

/*
Now let's talk about adding your own custom Dashboard widget.
Sometimes you want to show clients feeds relative to their
site's content. For example, the NBA.com feed for a sports
site. Here is an example Dashboard Widget that displays recent
entries from an RSS Feed.

For more information on creating Dashboard Widgets, view:
http://digwp.com/2010/10/customize-wordpress-dashboard/
*/

// RSS Dashboard Widget
function bones_rss_dashboard_widget() {
	if(function_exists('fetch_feed')) {
		include_once(ABSPATH . WPINC . '/feed.php');               // include the required file
		$feed = fetch_feed('http://themble.com/feed/rss/');        // specify the source feed
		$limit = $feed->get_item_quantity(7);                      // specify number of items
		$items = $feed->get_items(0, $limit);                      // create an array of items
	}
	if ($limit == 0) echo '<div>The RSS Feed is either empty or unavailable.</div>';   // fallback message
	else foreach ($items as $item) { ?>

	<h4 style="margin-bottom: 0;">
		<a href="<?php echo $item->get_permalink(); ?>" title="<?php echo $item->get_date('j F Y @ g:i a'); ?>" target="_blank">
			<?php echo $item->get_title(); ?>
		</a>
	</h4>
	<p style="margin-top: 0.5em;">
		<?php echo substr($item->get_description(), 0, 200); ?>
	</p>
	<?php }
}

// calling all custom dashboard widgets
function bones_custom_dashboard_widgets() {
	
	//wp_add_dashboard_widget('bones_rss_dashboard_widget', 'Recently on Themble (Customize on admin.php)', 'bones_rss_dashboard_widget');
	
	/*
	Be sure to drop any other created Dashboard Widgets
	in this function and they will all load.
	*/
}
// removing the dashboard widgets
add_action('admin_menu', 'disable_default_dashboard_widgets');
// adding any custom widgets
add_action('wp_dashboard_setup', 'bones_custom_dashboard_widgets');


/************* CUSTOM LOGIN PAGE *****************/

// calling your own login css so you can style it

// Updated to proper 'enqueue' method
// http://codex.wordpress.org/Plugin_API/Action_Reference/login_enqueue_scripts
function bones_login_css() {
	wp_enqueue_style( 'bones_login_css', get_template_directory_uri() . '/library/css/login.css', false );
}

// changing the logo link from wordpress.org to your site
function bones_login_url() { return home_url(); }

// changing the alt text on the logo to show your site name
function bones_login_title() { return get_option('blogname'); }

// calling it only on the login page
add_action( 'login_enqueue_scripts', 'bones_login_css', 10 );
add_filter( 'login_headerurl', 'bones_login_url' );
add_filter( 'login_headertitle', 'bones_login_title' );


/************* CUSTOMIZE ADMIN *******************/

// remove unnecessary admin menus  
function hide_menu_items() {
	if( current_user_can( 'update_core' ) ):
		remove_menu_page( 'edit-comments.php' ); // Comments
	endif;
	if( !current_user_can( 'update_core' ) ):
		remove_menu_page( 'index.php' ); // Dashboard tab
		remove_menu_page( 'edit.php?post_type=page '); // Pages
		remove_menu_page( 'upload.php' ); // Media
		remove_menu_page( 'link-manager.php' ); // Links
		remove_menu_page( 'edit-comments.php' ); // Comments
		remove_menu_page( 'themes.php' ); // Appearance
		remove_menu_page( 'plugins.php' ); // Plugins
		remove_menu_page( 'users.php' ); // Users
		remove_menu_page( 'tools.php' ); // Tools
		remove_menu_page( 'options-general.php' ); // Settings
		// remove_menu_page( 'edit.php?post_type=slider_type' );
		// remove_menu_page( 'acf-options' );
	endif;
	}
add_action( 'admin_menu', 'hide_menu_items', 999 );


// Disable the Wordpress Admin Bar for all but admins.
if (!current_user_can('update_core')):
	show_admin_bar(false);
endif;


// Limit Number of Post & Page Revisions
add_filter( 'wp_revisions_to_keep', 'limit_revisions_by_posttype', 10, 2 );
function limit_revisions_by_posttype( $num, $post ) {
	$num = 15;
	return $num;
}


// This theme styles the visual editor with editor-style.css to match the theme style.
add_editor_style();


// TinyMCE edits: add visualblock plugin
add_filter('mce_external_plugins', 'add_mceplugins');
function add_mceplugins() {
	$plugins = array('visualblocks');
	$plugins_array = array();

	foreach ($plugins as $plugin ) {
		$plugins_array[ $plugin ] = get_stylesheet_directory_uri() . '/library/js/'. $plugin .'/plugin.js';
	}
	return $plugins_array;
}
function myformatTinyMCE($in) {
	$in['toolbar2'].=',visualblocks ';
	$in['visualblocks_default_state'] = true;
	return $in;
}
add_filter('tiny_mce_before_init', 'myformatTinyMCE' );


// Kitchen sink (toolbar2) has good stuff, let's show it by default
function unhide_kitchensink( $args ) {
	$args['wordpress_adv_hidden'] = false;
	return $args;
}
add_filter( 'tiny_mce_before_init', 'unhide_kitchensink' );


// First, create a function that includes the path to your favicon
function add_favicon() {
	$favicon_url = '/favicon.png';
	echo '<link rel="icon" type="image/png" href="' . $favicon_url . '" sizes="32x32" />';
}
// Now, just make sure that function runs when you're on the login page and admin pages
add_action('login_head', 'add_favicon');
add_action('admin_head', 'add_favicon');


/* 
// Custom Backend Footer
function bones_custom_admin_footer() {
	echo '<span id="footer-thankyou">Developed by <a href="http://yoursite.com" target="_blank">Company Name</a></span>.';
}

// adding it to the admin area
add_filter('admin_footer_text', 'bones_custom_admin_footer');
*/

?>