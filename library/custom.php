<?php

// gravity forms hide lable option
add_filter( 'gform_enable_field_label_visibility_settings', '__return_true' );

//ACF Options page
if( function_exists('acf_add_options_page') ) {

	acf_add_options_page();

}

// DETETERMINE IF A PAGE IS WITHIN A TREE
function is_tree( $pid ) {      // $pid = The ID of the page we're looking for pages underneath
	global $post;               // load details about this page

	if ( is_page($pid) )
		return true;            // we're at the page or at a sub page

	$anc = get_post_ancestors( $post->ID );
	foreach ( $anc as $ancestor ) {
		if( is_page() && $ancestor == $pid ) {
			return true;
		}
	}
	return false;  // we arn't at the page, and the page is not an ancestor
}

// FIX BLANK WP SEARCH BUG
function search_query_filter($query) {
   // If 's' request variable is set but empty
   if (isset($_GET['s']) && empty($_GET['s']) && $query->is_main_query()){
	  $query->is_search = true;
	  $query->is_home = false;
   }
   return $query;

}
add_filter('pre_get_posts', 'search_query_filter');


// CUSTOM TITLE LENGTH
/* usage: <?php echo title_crop(55); ?> */
function title_crop($count){
	$title = get_the_title();
	if (strlen($title) > $count) {
	$title = substr($title, 0, $count) . '...';
	}
	return $title;
}

// Responsive Videos
add_filter('embed_oembed_html', 'my_embed_oembed_html', 99, 4);
function my_embed_oembed_html($html, $url, $attr, $post_id) {
  return '<div class="video-container">' . $html . '</div>';
}


// (OPT) STOP SHORTCODES THAT DON'T USE INLINE CONTENT FROM BEING WRAPPED IN A P TAG (until WP fixes this)
// ** NOTE -> BE SURE TO change the array to the shortcodes you are using!
add_filter('the_content', 'the_content_filter');
function the_content_filter($content) {
	// array of custom shortcodes requiring the fix
	$block = join("|",array( 'blockquote', 'row', 'column' ));
	// opening tag
	$rep = preg_replace("/(<p>)?\[($block)(\s[^\]]+)?\](<\/p>|<br \/>)?/","[$2$3]",$content);
	// closing tag
	$rep = preg_replace("/(<p>)?\[\/($block)](<\/p>|<br \/>)?/","[/$2]",$rep);
	return $rep;
}


// (OPT) ALLOW SHORTCODES IN TEXT WIDGETS & STOP IT BEING WRAPPED IN A P TAG IN THE WIDGET
add_filter( 'widget_text', 'shortcode_unautop');
add_filter( 'widget_text', 'do_shortcode', 11);


// ADD A PARENT CLASS TO wp_list_pages (usually used for drop down navigation)
function add_parent_class( $css_class, $page, $depth, $args )
{
	if ( ! empty( $args['has_children'] ) )
		$css_class[] = 'parent-menu-item';
	return $css_class;
}
add_filter( 'page_css_class', 'add_parent_class', 10, 4 );


// (OPT) ADD A PARENT CLASS TO wp_nav_menu (usually used for drop down navigation)
function parent_menu_css_class( $classes, $item, $args ) {
	$menu_items = wp_get_nav_menu_items( $args->menu );
	if ( ! $menu_items && $args->theme_location && ( $locations = get_nav_menu_locations() ) && isset( $locations[ $args->theme_location ] ) ) :
	$menu = wp_get_nav_menu_object( $locations[ $args->theme_location ] );
	$menu_items = wp_get_nav_menu_items( $menu->term_id );
	endif;
	foreach ( $menu_items as $menu_item ) :
		if ( $menu_item->menu_item_parent == $item->ID ) :
			$classes[] = 'parent-menu-item';
			break;
		endif;
	endforeach;
	return $classes;
 }
add_filter( 'nav_menu_css_class', 'parent_menu_css_class', 10, 3 );

/**
 * Attach a class to linked images' parent anchors
 */
function give_linked_images_class($html, $id, $caption, $title, $align, $url, $size, $alt = '' ){
	$classes = 'magnific'; // separated by spaces, e.g. 'img image-link'

	// check if there are already classes assigned to the anchor
	if ( preg_match('/<a.*? class=".*?">/', $html) ) {
		$html = preg_replace('/(<a.*? class=".*?)(".*?>)/', '$1 ' . $classes . '$2', $html);
	} else {
		$html = preg_replace('/(<a.*?)>/', '$1 class="' . $classes . '" >', $html);
	}
	return $html;
	}
add_filter('image_send_to_editor','give_linked_images_class',10,8);




// ***** CUSTOM SHORTCODES ***********************

// Intro P tag shortcode
function bbones_featured_text($atts, $content=null, $code="") {
	$return = '<p class="featured-text">';
	$return .= $content;
	$return .= '</p>';
	return $return;
}
add_shortcode('featured-text' , 'bbones_featured_text' );


// Feature Button shortcode
function bbones_link_button($atts, $content=null, $code="") {
	extract(shortcode_atts(array(
		'color' => 'white','orange','green',
	), $atts));
	return '<span class="shortcode-button '. $color . '">' . do_shortcode($content) . '</span>';
}
add_shortcode('button', 'bbones_link_button');



// Blockquote shortcode
function bbones_bquote($atts, $content = null) {
	extract(shortcode_atts(array(
		'cite' => 'Unknown',
		'url' => 'url'
	), $atts));
	if ( $cite == 'Unknown' ) {
		$return = '<blockquote>'. do_shortcode($content) .'</blockquote>';
	} elseif ( $url == 'url' ) {
		$return = '<blockquote>'. do_shortcode($content) .'<p>'. $cite .'</p></blockquote>';
	} else {
		$return = '<blockquote><p>'. do_shortcode($content) .'</p><p><a href="'. $url .'">'. $cite .'</a></p></blockquote>';
	}
	return $return;
}
add_shortcode('blockquote', 'bbones_bquote');


// ***** END SHORTCODES **********************

add_filter( 'image_size_names_choose', 'my_custom_sizes' );

function my_custom_sizes( $sizes ) {
    return array_merge( $sizes, array(
        'crop-800' => __( 'Soft 800' ),
				'crop-628' => __( 'Large Square' )
    ) );
}


?>
