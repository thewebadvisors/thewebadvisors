<?php
/*
Template Name: CPT Items Home Page
*/
?>

<?php get_header(); ?>
			
	<div class="main-content">
	
		<div class="inner-content  container  clearfix">
	
			<div class="primary-content  clearfix" role="main">

				<h1 class="page-title">CPT Type Title If Hardcoded</h1>

				<?php
					$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
					$args = array( 
						'posts_per_page' => 1, 
						'paged' => $paged, 
						'post_type' => 'cpt_type'
					);
					$cpt_query = new WP_Query($args);
				?>
				
				<?php if ($cpt_query->have_posts()) : while ($cpt_query->have_posts()) : $cpt_query->the_post(); ?>
					
				<article <?php post_class('clearfix'); ?> role="article">

					<section class="entry-content  clearfix">
						
						<h2 class="single-title"><?php the_title(); ?></h2>

						<?php the_excerpt(); ?>

					</section> <!-- /entry-content -->
			
				</article> <!-- /article -->

				<?php endwhile; endif; ?>

				<?php /* Display navigation to next/previous pages when applicable */ ?>
				<?php if ( $cpt_query->max_num_pages > 1 ) : ?>
					<?php $max_page = $cpt_query->max_num_pages; ?>
					<nav class="wp-prev-next">
						<ul class="clearfix">
							<li class="prev-link"><?php previous_posts_link( '&laquo; PREV', $cpt_query->max_num_pages) ?></li> 
							<?php if (($paged < $max_page) && ($paged > 1))  { echo "<li style='float:left;'><span>|</span></li>"; }  ?> 
							<li class="next-link"><?php next_posts_link( 'NEXT &raquo;', $cpt_query->max_num_pages) ?></li>
						</ul>
					</nav>
				<?php endif; ?>	

				<?php wp_reset_postdata(); ?>   
	
			</div> <!-- /primary-content -->

			<?php get_sidebar(); // sidebar ?>
			
		</div> <!-- /inner-content -->

	</div> <!-- /main-content -->

<?php get_footer(); ?>