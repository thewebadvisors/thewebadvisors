<?php get_header(); ?>
			
		<div class="main-content">

			<div class="inner-content  container  clearfix">
		
				<div class="primary-content  clearfix" role="main">	
					
					<h1>Sorry - This Page Does Not Exist! (404)</h1>
			
					<section class="entry-content">
					
						<p>The page you were looking for was not found. <br />Please try going to the <a href='/'>Home Page</a>.</p>
			
					</section> <!-- /article section -->
		
				</div> <!-- /primary-content -->

			</div> <!-- /inner-content -->

		</div> <!-- /main-content -->

<?php get_footer(); ?>
