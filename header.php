<!doctype html>

<!--[if lt IE 7]><html <?php language_attributes(); ?> class="no-js lt-ie9 lt-ie8 lt-ie7"><![endif]-->
<!--[if (IE 7)&!(IEMobile)]><html <?php language_attributes(); ?> class="no-js lt-ie9 lt-ie8"><![endif]-->
<!--[if (IE 8)&!(IEMobile)]><html <?php language_attributes(); ?> class="no-js lt-ie9"><![endif]-->
<!--[if gt IE 8]><!--> <html <?php language_attributes(); ?> class="no-js"><!--<![endif]-->

<head>
	<meta charset="utf-8">

	<!-- Google Chrome Frame for IE -->
	<!--[if IE]><meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"><![endif]-->

	<title>
	<?php
		if (!defined('WPSEO_VERSION')) {
			echo wp_title('|', true, 'right'); bloginfo('name');
		}
		else { // If WordPress SEO by Yoast is activated
			wp_title();
		}
	?>
	</title>

	<!-- mobile meta (hooray!) -->
	<meta name="viewport" content="width=device-width, initial-scale=1.0"/>


	<!-- favicons -->
	<link rel="icon" type="image/png" href="/favicon-hd.png" sizes="196x196">
	<link rel="icon" type="image/png" href="/favicon.png" sizes="32x32">
	<link rel="apple-touch-icon" href="/favicon-apple.png">

	<!-- other html head stuff (item to load before WP/theme scripts are loaded) -->
	<link href='http://fonts.googleapis.com/css?family=Raleway:200,400,300,600,700,800' rel='stylesheet' type='text/css'>
	<!-- wordpress head functions -->
	<?php wp_head(); ?>
	<!-- end of wordpress head -->


	<!-- drop Google Analytics Here -->
	<!-- end analytics -->

</head>

<body <?php body_class(pretty_body_class()); ?>>
<!-- Google Tag Manager -->
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-PDCXHT"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-PDCXHT');</script>
<!-- End Google Tag Manager -->

<!--[if lt IE 9]><p class="browserwarning">Your browser is <strong>very old.</strong> Please <a href="http://browsehappy.com/">upgrade to a different browser</a> to properly browse this web site.</p><![endif]-->
<div>
<nav id="mobile-menu">

					<?php bbones_nav_menu( 'main-nav', 'menu-main' ); ?>

</nav>

	<div class="page-header FixedTop"  role="banner">

		<div class="inner-header container clearfix">

			<p class="main-logo"><a href="<?php echo home_url(); ?>" rel="nofollow"><?php bloginfo('name'); ?></a></p>

			<?php // bloginfo('description');  // to use the site description you can un-comment it, if not DELETE this ?>


				<nav role="navigation" class="nav-main   clearfix">
					<?php bbones_nav_menu( 'main-nav', 'menu-main' ); ?>
				</nav>


			<a class="mobile-menu-button" href="#mobile-menu"><span class="icon-menu"></span><span>Menu</span></a>

		</div> <!-- /inner-header -->

	</div> <!-- /header -->
