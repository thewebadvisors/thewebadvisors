<?php get_header(); ?>

	<div class="main-content ">

		<div class="inner-content clearfix">

				<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

			<section class="entry-content  clearfix">
						<div class="page-title-wrap" <?php if (get_field('title_bg_image')) { ?> style="background-image:url(<?php the_field('title_bg_image') ?>)"<?php } ?>>
							<div class="title-wrap-overlay" <?php if (get_field('title_bg_image')) { ?> style="background: rgba(0,0,0,0.4);"<?php } ?>>
								<div class="container">
									<h1 class="page-title"<?php if (get_field('title_bg_image')) { ?>style="color: #fff;"<?php } ?> ><?php the_title(); ?></h1>
									<?php if (get_field('page_subtitle')) { ?><h3 <?php if (get_field('title_bg_image')) { ?>style="color: #fff;"<?php } ?>><?php the_field('page_subtitle') ?></h3><?php } ?>
								</div>
							</div>
						</div>

				<div class="primary-content  clearfix" role="main">


					<?php if (get_field('use_page_builder') == "no" ) { ?><div class="container"><?php the_content(); ?></div><?php } ?>

					<?php if (get_field('use_page_builder') == "yes" ) { ?>
						<?php

						// check if the flexible content field has rows of data
						if( have_rows('page_builder') ):

						     // loop through the rows of data
						    while ( have_rows('page_builder') ) : the_row();

						        if( get_row_layout() == '1_column_layout' ):

						        	echo '<div class="container"><div class="row"><div class="col-md-12">';
						        		the_sub_field('full_width_layout');
						        	echo '</div></div></div>';

						        elseif( get_row_layout() == '2_column_layout' ):

						        	echo '<div class="container"><div class="row"><div class="col-md-6">';
						        		the_sub_field('column_1_of_2');
						        	echo '</div>';
						        	echo '<div class="col-md-6">';
						        		the_sub_field('column_2_of_2');
						        	echo '</div></div></div>';


						        elseif( get_row_layout() == '3_column_layout' ):

						        	echo '<div class="container"><div class="row"><div class="col-md-4">';
						        		the_sub_field('column_1_of_3');
						        	echo '</div>';
						        	echo '<div class="col-md-4">';
						        		the_sub_field('column_2_of_3');
						        	echo '</div>';
						        	echo '<div class="col-md-4">';
						        		the_sub_field('column_3_of_3');
						        	echo '</div></div></div>';

						        elseif( get_row_layout() == '13_layout' ):

						        	echo '<div class="container"><div class="row"><div class="col-md-4">';
						        		the_sub_field('column_1-3');
						        	echo '</div>';
						        	echo '<div class="col-md-8">';
						        		the_sub_field('column_2-3');
						        	echo '</div></div></div>';

						        elseif( get_row_layout() == '23_layout' ):

						        	echo '<div class="container"><div class="row"><div class="col-md-8">';
						        		the_sub_field('column_2-3b');
						        	echo '</div>';
						        	echo '<div class="col-md-4">';
						        		the_sub_field('column_1-3b');
						        	echo '</div></div></div>';

						        elseif( get_row_layout() == 'divider_text' ):

						        	echo '<div class="container"><div class="divider"><h5><span>';
						        		the_sub_field('divider_title');
						        	echo '</sapn></h5></div></div>';

						         elseif( get_row_layout() == 'parallax_bg_section' ):

						        	echo '<div class="parallax-wrap"><div class="parallax" style="background-image: url('. get_sub_field('parallax_image') .')"></div></div>';





						        endif;

						    endwhile;

						else :

						    // no layouts found

						endif;

						?>
					<?php } ?>

			<!-- 		<div class="parallax-wrap">
						<div class="parallax" style="background-image: url(http://twa.localhost/wp-content/uploads/2014/11/sail.jpg);">

						</div>
					</div> -->



					</div> <!-- /primary-content -->

						<?php if (is_page(2863)) { ?>
							<?php get_template_part( 'parts/part', 'ratings' ); ?>
						<?php } ?>
				</section> <!-- /entry-content -->

				<?php endwhile; endif; // END main loop (if/while) ?>

				<?php get_template_part( 'parts/part', 'relatedposts' ); ?>



		</div> <!-- /inner-content -->

	</div> <!-- /main-content -->

<?php get_footer(); ?>
