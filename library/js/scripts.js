/*
BareBones Scripts File

This file should contain any js scripts you want to add to the site.
Instead of calling it in the header or throwing it inside wp_head()
this file will be called automatically in the footer so as not to
slow the page load.

*/
// allow for jQuery noConflict mode
( function( $ ) {

$('.isotope-port').isotope({
    itemSelector: '.item',
    masonry: {
      columnWidth: '.grid-sizer-port'
    }
});


$('.isotope').isotope({
    itemSelector: '.item',
    masonry: {
      columnWidth: '.grid-sizer'
    }
});


   // cache container
    var $container = $('.isotope-port');
// filter functions
  var filterFns = {
    // show if number is greater than 50
    numberGreaterThan50: function() {
      var number = $(this).find('.number').text();
      return parseInt( number, 10 ) > 50;
    },
  };
  // bind filter button click
  $('#filters').on( 'click', 'button', function() {
    var filterValue = $( this ).attr('data-filter');
    // use filterFn if matches value
    filterValue = filterFns[ filterValue ] || filterValue;
    $container.isotope({ filter: filterValue });
  });
  // change is-checked class on buttons
  $('.button-group').each( function( i, buttonGroup ) {
    var $buttonGroup = $( buttonGroup );
    $buttonGroup.on( 'click', 'button', function() {
      $buttonGroup.find('.is-checked').removeClass('is-checked');
      $( this ).addClass('is-checked');
    });
  });



// as the page loads, call these scripts
$(document).ready(function() {

	/*
	Responsive jQuery is a tricky thing.
	There's a bunch of different ways to handle
	it so be sure to research and find the one
	that works for you best.
	*/

	/* getting viewport width */
	var responsive_viewport = $(window).width();

	/* if is below 481px */
	if (responsive_viewport <= 481) {

	} /* end smallest screen */

	/* if is larger than 481px */
	if (responsive_viewport > 481) {

	} /* end larger than 481px */

	/* if is above or equal to 768px */
	if (responsive_viewport >= 768) {

		/* load gravatars */
		$('.comment img[data-gravatar]').each(function(){
			$(this).attr('src',$(this).attr('data-gravatar'));
		});

	} /* end larger than 768px */

	/* off the bat large screen actions */
	if (responsive_viewport > 1030) {

	} /* end larger than 1030px */

	/* END Responsive jQuery *************************/


	// PLACEHOLDER CSS3 for < IE10
	if (!Modernizr.input.placeholder) {
		$('input[type="text"]', 'header').each(function () {
			if (!$(this).val()) {
				this.value = $(this).attr('placeholder');
			}
			$(this).focus(function () {
				if (this.value == $(this).attr('placeholder')) {
					this.value = '';
				}
			});
			$(this).blur(function () {
				if (this.value == '') {
					this.value = $(this).attr('placeholder');
				}
			});
		});
	}


// *********************** START CUSTOM jQuery SCRIPTS *******************************

 $('.tooltip').tooltipster({});

$("#mobile-menu").mmenu({
         // options
         classes: "mm-dark",
          offCanvas: {
               position  : "right",
               zposition : "back"
           }
      });


	// init default magnificPopup setup for images

	$('.magnific').magnificPopup({
		type: 'image',
		closeOnContentClick: true,
		closeBtnInside: false,
		fixedContentPos: true,
		mainClass: 'mfp-no-margins mfp-with-zoom', // class to remove default margin from left and right side
		image: {
			verticalFit: true
		},
		zoom: {
			enabled: true,
			duration: 300 // don't foget to change the duration also in CSS
		}
	});

  $('.magnific-form').magnificPopup({
        type: 'inline',
    		preloader: false,

    });


$('.slider-block').cycle({
	speed: 1000,
	manualSpeed: 400,
	slides: '> .slide-item',
	timeout: 100000,
	swipe: true,
	autoHeight: 'calc',
	pager: ".spager",
	pagerTemplate : "<a href='#'><span class='sliderpager'></span></a>"
});




// *********************** END CUSTOM jQuery SCRIPTS *********************************


}); /*end of document.ready scripts */


})( jQuery ); /* end of no conflict area */
