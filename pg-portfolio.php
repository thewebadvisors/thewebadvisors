<?php
/*
Template Name: Portfolio HomePage
*/
?>

<?php get_header(); ?>

	<div class="main-content ">

		<div class="inner-content clearfix">

				<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

			<section class="entry-content  clearfix">
						<div class="page-title-wrap">
							<div class="title-wrap-overlay" >
								<div class="container">
									<h1 class="page-title"><?php the_title(); ?></h1>
									<?php if (get_field('page_subtitle')) { ?><h3 <?php if (get_field('title_bg_image')) { ?>style="color: #fff;"<?php } ?>><?php the_field('page_subtitle') ?></h3><?php } ?>
								</div>
							</div>
						</div>

				<div class="primary-content clearfix" role="main">


				<div class="container">
						<div class="row">
							<?php
								$terms = get_terms('portfolioclassification_tax');
								$count = count($terms);
									if ( $count > 0 ) {
										echo '<div id="filters" class="button-group">';
										echo '<button class="is-checked button" data-filter="*">All</button>';

									foreach ( $terms as $term ) {
										$termname = strtolower($term->name);
										$termname = str_replace(' ', '-', $termname);
										echo '<button class="button" data-filter="' . '.' . $termname . '"> ' . $term->name . '';
									}
									echo '</div>';
								}
							?>
						</div>
				</div>

					<?php
						$args = array(
							'posts_per_page' => -1,
							'post_type' => 'portfolio_type',
							'orderby' => 'rand'
						);
						$cpt_query = new WP_Query($args);

					?>

					<div class="isotopeliquid isotope-port">
							<div class="grid-sizer-port"></div>
						<?php if ($cpt_query->have_posts()) : while ($cpt_query->have_posts()) : $cpt_query->the_post(); ?>

						<?php $termsp = get_the_terms( $post->ID, 'portfolioclassification_tax' ); ?>
						<?php $url = wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); ?>

							<div class="<?php foreach( $termsp as $term ) echo ' ' . $term->slug   ;?> item " style="background: url(<?php echo $url;?>) no-repeat 50% 50%; background-size: cover;">
								<a href="<?php the_permalink();?>" class="overlay"><span class="isotope-port-title"><?php the_title(); ?></span>
									<div class="portfolio-cats clearfix">
										<ul ><?php foreach( $termsp as $term ) echo ' <li> ' . $term->name .' </li> '  ;?></ul>
									</div>
								</a>
							</div>

						<?php endwhile; endif; // end of CPT loop ?>
					</div>

					<?php wp_reset_postdata(); ?>


				</div> <!-- /primary-content -->

				</section> <!-- /entry-content -->

				<?php endwhile; endif; // END main loop (if/while) ?>

				<?php // IF USING PARTS -> get_template_part( 'parts/part', 'the-staff' ); ?>



		</div> <!-- /inner-content -->

	</div> <!-- /main-content -->

<?php get_footer(); ?>
