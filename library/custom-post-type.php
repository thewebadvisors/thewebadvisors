<?php
/* Super Bones Custom Post Types ****************************/

// Flush rewrite rules for custom post types
add_action( 'after_switch_theme', 'bones_flush_rewrite_rules' );

// Flush your rewrite rules
function bones_flush_rewrite_rules() {
	flush_rewrite_rules();
}

// ---- Register ??? CPT NAME ??? post type --------
function register_portfolio_type() {
	// Labels
	$singular = 'Portfolio Item';
	$plural = 'Portfolio Items';
	$labels = array(
		'name' => _x($singular, "post type general name"),
		'singular_name' => _x($singular, "post type singular name"),
		'all_items' => __("All $plural"),
		'menu_name' => $plural,
		'add_new' => _x("Add New", "team item"),
		'add_new_item' => __("Add New $singular"),
		'edit_item' => __("Edit $singular"),
		'new_item' => __("New $singular"),
		'view_item' => __("View $singular"),
		'search_items' => __("Search $plural"),
		'not_found' =>  __("No $plural Found"),
		'not_found_in_trash' => __("No $plural Found in Trash"),
		'parent_item_colon' => ''
	);

	// Register post type ** INSERT POST TYPE NAME BELOW **
	register_post_type('portfolio_type' , array(
		'labels' => $labels,
		'public' => true,
		'has_archive' => false, /* true or use custom slug => 'custom_type_url/past' */
		'menu_position' => 20,
		'menu_icon' => 'dashicons-book',
		'rewrite' =>  array( 'slug' => 'our-portfolio', 'with_front' => false ), 
		'supports' => array( 'title', 'editor', 'author', 'thumbnail', 'revisions')
	) );
}
add_action( 'init', 'register_portfolio_type', 0 );


// ---- Register ??? TAX NAME ??? taxonomy ----------------
function register_portfolioclassification_taxonomy() {
	// Labels
	$singular = 'Classification';
	$plural = 'Classifications';
	$labels = array(
		'name' => _x( $plural, "taxonomy general name"),
		'singular_name' => _x( $singular, "taxonomy singular name"),
		'search_items' =>  __("Search $singular"),
		'all_items' => __("All $plural"),
		'parent_item' => __("Parent $singular"),
		'parent_item_colon' => __("Parent $singular:"),
		'edit_item' => __("Edit $singular"),
		'update_item' => __("Update $singular"),
		'add_new_item' => __("Add New $singular"),
		'new_item_name' => __("New $singular Name"),
	);

	// Register and attach taxonomy to 'postname_type' post type ** INSERT TAXONOMY NAME BELOW **
	register_taxonomy( 'portfolioclassification_tax', 'portfolio_type', array(
		'public' => true,
		'show_ui' => true,
		'show_in_nav_menus' => true,
		'hierarchical' => true,
		'query_var' => true,
		'show_admin_column' => true,
		'rewrite' => false,  /*  true or use custom slug => array( 'slug' => 'custom-tag-slug', 'with_front' => false  ) */
		'labels' => $labels
	) );
}
add_action( 'init', 'register_portfolioclassification_taxonomy', 0 );


?>