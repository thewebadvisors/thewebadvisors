<?php get_header(); ?>

		<div class="main-content">

			<div class="page-title-wrap" >
				<div class="title-wrap-overlay" >
					<div class="container">
						<?php if (is_category()) { ?>
							<h1 class="archive-title">
								<span><?php _e("Posts Categorized:", "bonestheme"); ?></span> <?php single_cat_title(); ?>
							</h1>

						<?php } elseif (is_tag()) { ?>
							<h1 class="archive-title">
								<span><?php _e("Posts Tagged:", "bonestheme"); ?></span> <?php single_tag_title(); ?>
							</h1>

						<?php } elseif (is_author()) {
							global $post;
							$author_id = $post->post_author;
						?>
							<h1 class="archive-title">
								<span><?php _e("Posts By:", "bonestheme"); ?></span> <?php echo get_the_author_meta('display_name', $author_id); ?>
							</h1>

						<?php } elseif (is_day()) { ?>
							<h1 class="archive-title">
								<span><?php _e("Daily Archives:", "bonestheme"); ?></span> <?php the_time('l, F j, Y'); ?>
							</h1>

						<?php } elseif (is_month()) { ?>
							<h1 class="archive-title">
								<span><?php _e("Monthly Archives:", "bonestheme"); ?></span> <?php the_time('F Y'); ?>
							</h1>

						<?php } elseif (is_year()) { ?>
							<h1 class="archive-title">
								<span><?php _e("Yearly Archives:", "bonestheme"); ?></span> <?php the_time('Y'); ?>
							</h1>
						<?php } ?>
					</div>
				</div>
			</div>


			<div class="inner-content container clearfix">
				<div class="row">
					<div class="primary-content  col-md-7 clearfix" role="main">




							<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

							<article <?php post_class('clearfix'); ?> role="article">

								<section class="entry-content  blog  clearfix" itemprop="articleBody">

															<a href="<?php the_permalink(); ?>"> <?php the_post_thumbnail('crop-700-400'); ?></a>

															<div class="row">
																<div class="col-sm-2 ">
																	<div class="post-date"><span class="day"><?php the_time('d') ?></span><span class="month-year"><?php the_time('M y') ?></span></div>
																	<div class="post-meta">
																		<?php the_author_posts_link(); ?><?php comments_popup_link( 'Leave a Comment', '1 Comment', '% Comments' ); ?>
																	</div> <!-- /post-meta -->
																</div>
																<div class="col-sm-10">
																	<div class="category-list clearfix"><?php the_category(' '); ?></div>
																	<h2 class="single-title" itemprop="headline"><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h2>
																	<?php the_excerpt(); ?>
																</div>
															</div>


															<!--
															<?php global $more; $more = 0; ?>
															<?php the_content("Read More..."); ?>
															-->



														</section> <!-- /blog content section -->

							</article> <!-- /article -->

							<?php endwhile; ?>

							<nav class="wp-prev-next">
								<ul class="clearfix">
									<li class="prev-link"><?php next_posts_link(__('&laquo; Older Entries', 'bonestheme')) ?></li>
									<li class="next-link"><?php previous_posts_link(__('Newer Entries &raquo;', 'bonestheme')) ?></li>
								</ul>
							</nav>

							<?php else : ?>

								<article class="post-not-found hentry clearfix">
									<header class="article-header">
										<h2><?php _e("Oops, Post Not Found!", "bonestheme"); ?></h2>
									</header>
									<section class="entry-content">
										<p><?php _e("Uh Oh. Something is missing. Try double checking things.", "bonestheme"); ?></p>
									</section>
									<footer class="article-footer">
										<p><?php _e("This is the error message in the archive.php template.", "bonestheme"); ?></p>
									</footer>
								</article>

							<?php endif; ?>

						</div> <!-- /primary-content -->

						<div class="col-md-4 col-md-offset-1 ">
						<?php get_sidebar(); // sidebar ?>
						</div>


				</div>

			</div> <!-- /inner-content -->

		</div> <!-- /main-content -->

<?php get_footer(); ?>
