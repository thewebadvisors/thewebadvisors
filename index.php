<?php get_header(); ?>
		
		<div class="main-content">
		
			<div class="inner-content  container  clearfix">
		
				<div class="primary-content  clearfix" role="main">

					<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
				
					<article <?php post_class('clearfix'); ?> role="article">
					
						<header class="article-header">
						
							<h2><a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></h2>
						
							<p class="byline vcard"><?php _e('Posted', 'bonestheme'); ?> <time datetime="<?php the_time('Y-m-j'); ?>" pubdate><?php the_time(get_option('date_format')); ?></time> <?php _e('by', 'bonestheme'); ?> <?php the_author_posts_link(); ?> <span class="amp">&</span> <?php _e('filed under', 'bonestheme'); ?> <?php the_category(', '); ?>.</p>
					
						</header> <!-- /article header -->
				
						<section class="entry-content clearfix">
							<?php the_content(); ?>
						</section> <!-- /article section -->
					
						<footer class="article-footer">

							<p class="tags"><?php the_tags('<span class="tags-title">Tags:</span> ', ', ', ''); ?></p>

						</footer> <!-- /article footer -->
						
						<?php // comments_template(); // uncomment if you want to use them ?>
				
					</article> <!-- /article -->
				
					<?php endwhile; ?>	
				
					<nav class="wp-prev-next">
						<ul class="clearfix">
							<li class="prev-link"><?php next_posts_link(__('&laquo; Older Entries', 'bonestheme')) ?></li>
							<li class="next-link"><?php previous_posts_link(__('Newer Entries &raquo;', 'bonestheme')) ?></li>
						</ul>
					</nav>
				
					<?php else : ?>
					
						<article class="post-not-found hentry  clearfix">
							<header class="article-header">
								<h1><?php _e("Oops, Post Not Found!", "bonestheme"); ?></h1>
							</header>
							<section class="entry-content">
								<p><?php _e("Uh Oh. Something is missing. Try double checking things.", "bonestheme"); ?></p>
							</section>
							<footer class="article-footer">
								<p><?php _e("This is the error message in the index.php template.", "bonestheme"); ?></p>
							</footer>
						</article>
				
					<?php endif; ?>
		
				</div> <!-- /primary-content -->

				<?php get_sidebar(); // sidebar ?>
				
			</div> <!-- /inner-content -->

		</div> <!-- /main-content -->

<?php get_footer(); ?>
