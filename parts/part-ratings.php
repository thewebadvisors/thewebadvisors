<?php if( have_rows('reviews') ): ?>
  <?php while( have_rows('reviews') ): the_row(); ?>

  <div class="reviewSection">
    <div class="container">
      <?php
        if (get_sub_field('customer_image')) {
          $imageArray = get_sub_field('customer_image'); // Array returned by Advanced Custom Fields
          $imageAlt = $imageArray['alt'];
          $imageTitle = $imageArray['title'];
          $imageURL = $imageArray['url']; // Grab the full size version URL
          $imageCropURL = $imageArray['sizes']['crop-628']; // (sizes: thumbnail, medium, large or 'crop-size-name' as set in functions)
          // now show the image
          echo '<div class="col-sm-2"><img class="ratingImage" src="' . $imageCropURL . '" alt="' . $imageAlt .'" title="' . $imageTitle . '" /></div>';
        }
      ?>
      <div class="col-sm-10">
        <?php echo '<h2>'. get_sub_field('review_title') .' </h2>' ;?>
        <div class="rating <?php echo get_sub_field('rating'); ?>"></div>
        <p>
           <?php echo get_sub_field('review_content'); ?>
        </p>
        <p class="customer-info">
          <?php if( get_sub_field('customer_name') ) {
            echo  get_sub_field('customer_name');
          } ?>
          <?php if( get_sub_field('customer_company') ) {
            echo '<br/>'. get_sub_field('customer_company');
          } ?>
        </p>

      </div>

    </div>
  </div>

  <?php endwhile; ?>
<?php endif; ?>
