			<div class="secondary-content  clearfix" role="complementary">

				<div class="featured-posts ">
					<h5 class="sidebar-title">Featured Posts</h5>


					<?php
						$temp = $wp_query;
						$wp_query = null;
						$wp_query = new WP_Query(
							array(
									'posts_per_page' => 3,
									'paged' => $paged,
									'cat' => 122
								)
							);
					?>

					<?php if ($wp_query->have_posts()) : while ($wp_query->have_posts()) : $wp_query->the_post(); ?>

							<section class="entry-content sidebar-blog  clearfix" itemprop="articleBody">
									<div class="sidebar-blog-img"><a href="<?php the_permalink(); ?>"> <?php the_post_thumbnail('small-thumb'); ?></a></div>
									<h6 class="single-title" itemprop="headline"><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h6>
							</section> <!-- /blog content section -->

					<?php endwhile; endif; ?>


					<?php $wp_query = null; $wp_query = $temp; ?>
					<?php wp_reset_postdata(); ?>
				</div>

				<div class="category-listing">
					<h5 class="sidebar-title">Categories</h5>
					<ul class="clearfix">
							<?php wp_list_categories('orderby=name&title_li=&exclude=122'); ?>
					</ul>
				</div>


				<?php if ( is_active_sidebar( 'sidebar' ) ) : ?>
					<?php dynamic_sidebar( 'sidebar' ); ?>
				<?php endif; ?>

			</div> <!-- /secondary-content -->
