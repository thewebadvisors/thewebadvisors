<?php
/*
Template Name: Home Page Template
*/
?>

<?php get_header(); ?>

<div class="main-content ">

	<div class="inner-content clearfix">

		<div class="primary-content  clearfix" role="main">

			<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

			<section class="entry-content  clearfix">

			<div class="container clearfix">
				<div class="slider-block">

					<?php if( have_rows('text_slider') ): ?>
						<?php while( have_rows('text_slider') ): the_row(); ?>

						<div class="slide-item">
							<div class="slide-item__wrap">
							<div class="spager"></div>
								<div class="slide-item__text">
									<?php if (get_sub_field('slide_title')) { ?><h2 class="slide-title"><?php the_sub_field('slide_title') ?></h2><?php } ?>
									<?php if (get_sub_field('slide_text')) { ?><p class="slide-text"><?php the_sub_field('slide_text') ?></p><?php } ?>
									<?php if (get_sub_field('slide_link')) { ?><a href="<?php the_sub_field('slide_link'); ?>" class="slide-link"><?php the_sub_field('slide_link_text')?></a><?php } ?>

								</div> <!-- /slide-item__text -->
							</div> <!-- slide-item__wrap -->
						</div> <!-- /slide-item -->

						<?php endwhile; ?>
					<?php endif; ?>

				</div> <!-- /slider-block -->
			</div>

			<div class="isotopeliquid isotope">

				 <div class="grid-sizer"></div>
				 <div class="item reg" style="background: url(<?php echo get_field('grid_image_1'); ?>) no-repeat 50% 50%; background-size: cover;">
					 <a href="<?php echo get_field('grid_link_1'); ?>" class="overlay">
						 <div class="overlay-inner">
							 <?php
							   if (get_field('grid_logo_1')) {
							     $imageArray = get_field('grid_logo_1'); // Array returned by Advanced Custom Fields
							     $imageAlt = $imageArray['alt'];
							     $imageTitle = $imageArray['title'];
							     $imageURL = $imageArray['url']; // Grab the full size version URL
							     echo '<img src="' . $imageURL . '" alt="' . $imageAlt .'" title="' . $imageTitle . '" />';
							   }
							 ?>
							 <span><?php if( get_field('grid_overlay_text_1') ) { echo  get_field('grid_overlay_text_1'); } ?></span>

						 </div>
					 </a>
				 </div>

				 <div class="item width2" style="background: url(<?php echo get_field('grid_image_2'); ?>) no-repeat 50% 50%; background-size: cover;">
					 <a href="<?php echo get_field('grid_link_2'); ?>" class="overlay">
						 <div class="overlay-inner">
							 <?php
							 	if (get_field('grid_logo_2')) {
							 		$imageArray = get_field('grid_logo_2'); // Array returned by Advanced Custom Fields
							 		$imageAlt = $imageArray['alt'];
							 		$imageTitle = $imageArray['title'];
							 		$imageURL = $imageArray['url']; // Grab the full size version URL
							 		echo '<img src="' . $imageURL . '" alt="' . $imageAlt .'" title="' . $imageTitle . '" />';
							 	}
							 ?>
							  <span><?php if( get_field('grid_overlay_text_2') ) { echo  get_field('grid_overlay_text_2'); } ?></span>
						 </div>
					 </a>
				 </div>

				 <div class="item small" style="background: url(<?php echo get_field('grid_image_3'); ?>) no-repeat 50% 50%; background-size: cover;">
					 <a href="<?php echo get_field('grid_link_3'); ?>" class="overlay">
						 <div class="overlay-inner">
							 <?php
							 	if (get_field('grid_logo_3')) {
							 		$imageArray = get_field('grid_logo_3'); // Array returned by Advanced Custom Fields
							 		$imageAlt = $imageArray['alt'];
							 		$imageTitle = $imageArray['title'];
							 		$imageURL = $imageArray['url']; // Grab the full size version URL
							 		echo '<img src="' . $imageURL . '" alt="' . $imageAlt .'" title="' . $imageTitle . '" />';
							 	}
							 ?>
							 <span><?php if( get_field('grid_overlay_text_3') ) { echo  get_field('grid_overlay_text_3'); } ?></span>
						 </div>
					 </a>
				 </div>

				 <div class="item reg" style="background: url(<?php echo get_field('grid_image_4'); ?>) no-repeat 50% 50%; background-size: cover;">
					 <a href="<?php echo get_field('grid_link_4'); ?>" class="overlay">
						 <div class="overlay-inner">
							 <?php
							 	if (get_field('grid_logo_4')) {
							 		$imageArray = get_field('grid_logo_4'); // Array returned by Advanced Custom Fields
							 		$imageAlt = $imageArray['alt'];
							 		$imageTitle = $imageArray['title'];
							 		$imageURL = $imageArray['url']; // Grab the full size version URL
							 		echo '<img src="' . $imageURL . '" alt="' . $imageAlt .'" title="' . $imageTitle . '" />';
							 	}
							 ?>
							  <span><?php if( get_field('grid_overlay_text_4') ) { echo  get_field('grid_overlay_text_4'); } ?></span>
						 </div>
					 </a>
				 </div>

				 <div class="item reg" style="background: url(<?php echo get_field('grid_image_5'); ?>) no-repeat 50% 50%; background-size: cover;">
					 <a href="<?php echo get_field('grid_link_5'); ?>" class="overlay">
						 <div class="overlay-inner">
							 <?php
							 	if (get_field('grid_logo_5')) {
							 		$imageArray = get_field('grid_logo_5'); // Array returned by Advanced Custom Fields
							 		$imageAlt = $imageArray['alt'];
							 		$imageTitle = $imageArray['title'];
							 		$imageURL = $imageArray['url']; // Grab the full size version URL
							 		echo '<img src="' . $imageURL . '" alt="' . $imageAlt .'" title="' . $imageTitle . '" />';
							 	}
							 ?>
							   <span><?php if( get_field('grid_overlay_text_5') ) { echo  get_field('grid_overlay_text_5'); } ?></span>
						 </div>
					 </a>
				 </div>

				 <div class="item reg" style="background: url(<?php echo get_field('grid_image_6'); ?>) no-repeat 50% 50%; background-size: cover;">
					 <a href="<?php echo get_field('grid_link_6'); ?>" class="overlay">
						 <div class="overlay-inner">
							 <?php
							 	if (get_field('grid_logo_6')) {
							 		$imageArray = get_field('grid_logo_6'); // Array returned by Advanced Custom Fields
							 		$imageAlt = $imageArray['alt'];
							 		$imageTitle = $imageArray['title'];
							 		$imageURL = $imageArray['url']; // Grab the full size version URL
							 		echo '<img src="' . $imageURL . '" alt="' . $imageAlt .'" title="' . $imageTitle . '" />';
							 	}
							 ?>
							<span><?php if( get_field('grid_overlay_text_6') ) { echo  get_field('grid_overlay_text_6'); } ?></span>
						 </div>
					 </a>
				 </div>

				 <div class="item reg" style="background: url(<?php echo get_field('grid_image_10'); ?>) no-repeat 50% 50%; background-size: cover;">
					 <a href="<?php echo get_field('grid_link_10'); ?>" class="overlay">
						 <div class="overlay-inner">
							 <?php
							 	if (get_field('grid_logo_10')) {
							 		$imageArray = get_field('grid_logo_10'); // Array returned by Advanced Custom Fields
							 		$imageAlt = $imageArray['alt'];
							 		$imageTitle = $imageArray['title'];
							 		$imageURL = $imageArray['url']; // Grab the full size version URL
							 		echo '<img src="' . $imageURL . '" alt="' . $imageAlt .'" title="' . $imageTitle . '" />';
							 	}
							 ?>
							 	<span><?php if( get_field('grid_overlay_text_10') ) { echo  get_field('grid_overlay_text_10'); } ?></span>
						 </div>
					 </a>
				 </div>

				 <div class="item small" style="background: url(<?php echo get_field('grid_image_7'); ?>) no-repeat 50% 50%; background-size: cover;">
					 <a href="<?php echo get_field('grid_link_7'); ?>" class="overlay">
						 <div class="overlay-inner">
							 <?php
							 	if (get_field('grid_logo_7')) {
							 		$imageArray = get_field('grid_logo_7'); // Array returned by Advanced Custom Fields
							 		$imageAlt = $imageArray['alt'];
							 		$imageTitle = $imageArray['title'];
							 		$imageURL = $imageArray['url']; // Grab the full size version URL
							 		echo '<img src="' . $imageURL . '" alt="' . $imageAlt .'" title="' . $imageTitle . '" />';
							 	}
							 ?>
							 	<span><?php if( get_field('grid_overlay_text_7') ) { echo  get_field('grid_overlay_text_7'); } ?></span>
						 </div>
					 </a>
				 </div>

				 <div class="item small" style="background: url(<?php echo get_field('grid_image_8'); ?>) no-repeat 50% 50%; background-size: cover;">
					 <a href="<?php echo get_field('grid_link_8'); ?>" class="overlay">
						 <div class="overlay-inner">
							 <?php
							 	if (get_field('grid_logo_8')) {
							 		$imageArray = get_field('grid_logo_8'); // Array returned by Advanced Custom Fields
							 		$imageAlt = $imageArray['alt'];
							 		$imageTitle = $imageArray['title'];
							 		$imageURL = $imageArray['url']; // Grab the full size version URL
							 		echo '<img src="' . $imageURL . '" alt="' . $imageAlt .'" title="' . $imageTitle . '" />';
							 	}
							 ?>
								<span><?php if( get_field('grid_overlay_text_8') ) { echo  get_field('grid_overlay_text_8'); } ?></span>
						 </div>
					 </a>
				 </div>

				 <div class="item small" style="background: url(<?php echo get_field('grid_image_9'); ?>) no-repeat 50% 50%; background-size: cover;">
					 <a href="<?php echo get_field('grid_link_9'); ?>" class="overlay">
						 <div class="overlay-inner">
							 <?php
							 	if (get_field('grid_logo_9')) {
							 		$imageArray = get_field('grid_logo_9'); // Array returned by Advanced Custom Fields
							 		$imageAlt = $imageArray['alt'];
							 		$imageTitle = $imageArray['title'];
							 		$imageURL = $imageArray['url']; // Grab the full size version URL
							 		echo '<img src="' . $imageURL . '" alt="' . $imageAlt .'" title="' . $imageTitle . '" />';
							 	}
							 ?>
							 	<span><?php if( get_field('grid_overlay_text_9') ) { echo  get_field('grid_overlay_text_9'); } ?></span>
						 </div>
					 </a>
				 </div>

			</div> <!--isotope-->

			<div class="mobile-grid clearfix">

				<div class="item mobile-full" style="background: url(<?php echo get_field('grid_image_m1'); ?>) no-repeat 50% 50%; background-size: cover;">
					<a href="<?php echo get_field('grid_link_m1'); ?>" class="overlay">
						<div class="overlay-inner">
							<?php
							 if (get_field('grid_logo_m1')) {
								 $imageArray = get_field('grid_logo_m1'); // Array returned by Advanced Custom Fields
								 $imageAlt = $imageArray['alt'];
								 $imageTitle = $imageArray['title'];
								 $imageURL = $imageArray['url']; // Grab the full size version URL
								 echo '<img src="' . $imageURL . '" alt="' . $imageAlt .'" title="' . $imageTitle . '" />';
							 }
							?>
							 <span><?php if( get_field('grid_overlay_text_m1') ) { echo  get_field('grid_overlay_text_m1'); } ?></span>
						</div>
					</a>
				</div>

				<div class="item mobile-half" style="background: url(<?php echo get_field('grid_image_m2'); ?>) no-repeat 50% 50%; background-size: cover;">
					<a href="<?php echo get_field('grid_link_m1'); ?>" class="overlay">
						<div class="overlay-inner">
							<?php
							 if (get_field('grid_logo_m2')) {
								 $imageArray = get_field('grid_logo_m2'); // Array returned by Advanced Custom Fields
								 $imageAlt = $imageArray['alt'];
								 $imageTitle = $imageArray['title'];
								 $imageURL = $imageArray['url']; // Grab the full size version URL
								 echo '<img src="' . $imageURL . '" alt="' . $imageAlt .'" title="' . $imageTitle . '" />';
							 }
							?>
							 <span><?php if( get_field('grid_overlay_text_m2') ) { echo  get_field('grid_overlay_text_m2'); } ?></span>
						</div>
					</a>
				</div>

				<div class="item mobile-half" style="background: url(<?php echo get_field('grid_image_m3'); ?>) no-repeat 50% 50%; background-size: cover;">
					<a href="<?php echo get_field('grid_link_m3'); ?>" class="overlay">
						<div class="overlay-inner">
							<?php
							 if (get_field('grid_logo_m3')) {
								 $imageArray = get_field('grid_logo_m3'); // Array returned by Advanced Custom Fields
								 $imageAlt = $imageArray['alt'];
								 $imageTitle = $imageArray['title'];
								 $imageURL = $imageArray['url']; // Grab the full size version URL
								 echo '<img src="' . $imageURL . '" alt="' . $imageAlt .'" title="' . $imageTitle . '" />';
							 }
							?>
							 <span><?php if( get_field('grid_overlay_text_m3') ) { echo  get_field('grid_overlay_text_m3'); } ?></span>
						</div>
					</a>
				</div>

			</div><!-- mobile-grid -->


			<div class="cta-section container">
				<div class="row">
					<div class="divider"><h5><span>Our Services</span></h5></div>

					<?php if( have_rows('our_services') ): ?>
							<?php $counter = 1 ?>
					  <?php while( have_rows('our_services') ): the_row(); ?>

							<div class="col-md-3 cta cta<?php echo $counter ?>">
								<a class="iconlink" href="<?php if( get_sub_field('cta_link') ) { echo  get_sub_field('cta_link') ;} ?>"><span class="cta-icon icon-<?php the_sub_field('cta_icon'); ?>"></span></a>

								<a class="cta-h2link" href="<?php if( get_sub_field('cta_link') ) { echo  get_sub_field('cta_link') ;} ?>"><h2><?php if( get_sub_field('cta_title_line_1') ) { echo  get_sub_field('cta_title_line_1') ;} ?><?php if( get_sub_field('cta_title_line_2') ) { echo '<br />'. get_sub_field('cta_title_line_2') ;} ?></h2></a>
								<?php if( get_sub_field('cta_content') ) {
								  echo '<p>' . get_sub_field('cta_content') . '</p>';
								} ?>
								<a class="cta-button" href="<?php if( get_sub_field('cta_link') ) { echo  get_sub_field('cta_link') ;} ?>"><?php if( get_sub_field('cta_title_line_1') ) { echo  get_sub_field('cta_title_line_1') ;} ?> <?php if( get_sub_field('cta_title_line_2') ) { echo  get_sub_field('cta_title_line_2') ;} ?></a>
							</div>

							<?php $counter ++ ?>
					  <?php endwhile; ?>
					<?php endif; ?>

				</div>
			</div>

			<?php if (get_field('hp_p_image')) { ?>
						<div class="parallax-wrap">
							<div class="parallax" style="background-image: url('<?php the_field('hp_p_image') ?>')"></div>
						</div>
			<?php } ?>



			<div class="testimonial-section container">
				<div class="row">
					<div class="divider"><h5><span>What Our Clients Say</span></h5></div>
					<div class="col-md-6">

						<blockquote>
							<?php if( get_field('testimonial_1_content') ) {
							  echo '<p>' . get_field('testimonial_1_content') . '</p>';
							} ?>
							<?php if( get_field('testimonial_1_author') ) {
							  echo '<p>' . get_field('testimonial_1_author') . '</p>';
							} ?>
						</blockquote>
					</div>
					<div class="col-md-6">
						<blockquote>
							<?php if( get_field('testimonial_2_content') ) {
								echo '<p>' . get_field('testimonial_2_content') . '</p>';
							} ?>
							<?php if( get_field('testimonial_2_author') ) {
								echo '<p>' . get_field('testimonial_2_author') . '</p>';
							} ?>
						</blockquote>
					</div>
				</div>
			</div>







			</section> <!-- /entry-content -->

			<?php endwhile; endif; ?> <!-- /end main loop (if/while) -->

		</div> <!-- /primary-content -->

	</div> <!-- /inner-content -->

</div> <!-- /main-content -->

<?php get_footer(); ?>
