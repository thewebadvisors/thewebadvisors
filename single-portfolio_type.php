<?php get_header(); ?>

		<div class="main-content">

			<div class="inner-content   clearfix">

				<div class="primary-content clearfix" role="main">

					<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

						<article <?php post_class('clearfix'); ?> role="article" itemscope itemtype="http://schema.org/BlogPosting">

							<header class="article-header">

								<div class="container">
									<div class="row">
										<div class="col-md-12">

											<h1 class="single-title " itemprop="headline"><?php the_title(); ?></h1>
											<?php if (get_field('post_subtitle_text')) { ?><h2 class="blog-subtittle"><?php the_field('post_subtitle_text') ?></h2><?php } ?>
										</div>

									</div>
								</div>


								<!-- Get the URL of the featured image and use as full width  -->
								<?php if (get_field('full_width_banner_image_2')) { ?>
								<div class="post-banner" style="background-image: url( '<?php the_field('full_width_banner_image_2') ?>');">  </div>
								<?php } ?>

							</header> <!-- /article header -->



								<section class="entry-content container clearfix" itemprop="articleBody">

								<div class="row">
									<div class="col-md-3 portfolio-aside">
										<h5>Client</h5>
										<?php if (get_field('portfolio_client')) { ?><p><?php the_field('portfolio_client') ?></p><?php } ?>

										<h5>Services</h5>
											<?php $termsp = get_the_terms( $post->ID, 'portfolioclassification_tax' ); ?>
											<ul class="portfolio-listing-cats"><?php foreach( $termsp as $term ) echo '<li>' . $term->name .'</li>'  ;?></ul>

										<?php if (get_field('portfolio_date')) { ?>
											<h5>Date</h5>
											<p><?php the_field('portfolio_date') ?></p>
										<?php } ?>
										<!-- Go to www.addthis.com/dashboard to customize your tools -->
										<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-547a12170177124f" async="async"></script>
										<!-- Go to www.addthis.com/dashboard to customize your tools -->
										<div class="addthis_sharing_toolbox"></div>
									</div>
									<div class="col-md-9">
										<?php the_content(); ?>
									</div>

								</div>






								</section> <!-- /article section -->




							<footer class="article-footer container">


								<?php the_tags('<p class="tags"><span class="tags-title">Tags:</span> ', ', ', '</p>'); ?>




							</footer> <!-- /article footer -->



						</article> <!-- /article -->

					<?php endwhile; else : ?>

						<article class="post-not-found  hentry  clearfix">
				    		<header class="article-header">
				    			<h1><?php _e("Oops, Post Not Found!", "bonestheme"); ?></h1>
				    		</header>
				    		<section class="entry-content">
				    			<p><?php _e("Uh Oh. Something is missing. Try double checking things.", "bonestheme"); ?></p>
				    		</section>
				    		<footer class="article-footer">
				    		    <p><?php _e("This is the error message in the single.php template.", "bonestheme"); ?></p>
				    		</footer>
						</article>

					<?php endif; ?>

				</div> <!-- /primary-content -->


			</div> <!-- /inner-content -->

		</div> <!-- /main-content -->

<?php get_footer(); ?>
