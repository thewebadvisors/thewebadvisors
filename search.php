<?php get_header(); ?>
			
		<div class="main-content">

			<div class="inner-content  container  clearfix">
		
				<div class="primary-content  clearfix" role="main">
			
					<h1 class="archive-title"><span>Search Results for:</span> <?php echo esc_attr(get_search_query()); ?></h1>

					<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
				
						<article <?php post_class('clearfix'); ?> role="article">
					
							<header class="article-header">
								<h3 class="search-title"><a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></h3>
								<p class="byline vcard"><?php _e("Posted", "bonestheme"); ?> <time datetime="<?php echo the_time('Y-m-j'); ?>" pubdate><?php the_time('F jS, Y'); ?></time> <?php _e("by", "bonestheme"); ?> <?php the_author_posts_link(); ?> <span class="amp">&</span> <?php _e("filed under", "bonestheme"); ?> <?php the_category(', '); ?>.</p>
							</header> <!-- /article header -->
				
							<section class="entry-content">
							    <?php the_excerpt(); ?>
							</section> <!-- /article section -->

						</article> <!-- /article -->
				
					<?php endwhile; ?>	

				        <nav class="wp-prev-next">
					        <ul class="clearfix">
						        <li class="prev-link"><?php next_posts_link(__('&laquo; Older Entries', "bonestheme")) ?></li>
						        <li class="next-link"><?php previous_posts_link(__('Newer Entries &raquo;', "bonestheme")) ?></li>
					        </ul>
				        </nav>	
				
				    <?php else : ?>
				
					    <article class="post-not-found hentry  clearfix">
					    	<header class="article-header">
					    		<h1><?php _e("Sorry, No Results.", "bonestheme"); ?></h1>
					    	</header>
					    	<section class="entry-content">
					    		<p><?php _e("Please try another search.", "bonestheme"); ?></p>
					    	</section>
					    </article>
				
				    <?php endif; ?>
		
			    </div> <!-- /primary-content -->
			
			    <?php get_sidebar(); // sidebar ?>
			
			</div> <!-- /inner-content -->

		</div> <!-- /main-content -->

<?php get_footer(); ?>